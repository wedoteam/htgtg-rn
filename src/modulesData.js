export const data = {
    wmw:{
        title: 'What Men Want',
        hasposts: false,
        hascategories: false,
        data: {
            title: 'What Men Want: 7 Secrets to Pull Him Closer Instead of Pushing Him Away',
            content: '<div class="content-box"><p><a class="hidden-lg hidden-md" href="https://dl.howtogettheguy.com/whatmenwant/what_men_want-matthew_hussey.pdf"><img class="size-medium wp-image-2318 aligncenter" src="https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406-300x63.png" alt="" width="300" height="63" srcset="https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406-300x63.png 300w, https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406.png 603w" sizes="(max-width: 300px) 100vw, 300px"></a></p><p>Get ready…You’re about to finally discover <strong>What Men Want</strong>.</p><p>I’ve distilled <strong>my essential 7 secrets of male psychology</strong> (from years as a top love-life strategist coaching both men and women) so that you can take quick action to draw any man you choose to you just like a magnet.</p><p>In just a moment, I’m going to take you by the hand and walk you through those insanely tricky “early dating stages” so <u>you can avoid</u> the most common (and disastrous) mistakes almost every woman makes during this crucial time.</p><p>You’ll discover what men are looking for in a woman they’ll commit to, <strong>the real reasons why men disappear</strong> (even when things seem like they’re going great), and my secret formula that <strong>guarantees HE’LL keep making the effort to see YOU</strong> again after every date. (Try it; you’ll be amazed at how quickly it works!)</p><p>I’ll even reveal exactly how to hit the “sweet spot” of time spent together so <strong>he’ll become positively addicted to you.</strong></p><p>I know you’re as excited as I am, so let’s get started!</p><p><a href="https://dl.howtogettheguy.com/whatmenwant/what_men_want-matthew_hussey.pdf"><img class="size-medium wp-image-2318 aligncenter" src="https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406-300x63.png" alt="" width="300" height="63" srcset="https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406-300x63.png 300w, https://members.howtogettheguy.com/wp-content/uploads/2016/07/a825616da0b8a9161241f34447a40406.png 603w" sizes="(max-width: 300px) 100vw, 300px"></a><br><a href="https://dl.howtogettheguy.com/whatmenwant/what_men_want-matthew_hussey.pdf"><strong>Click Here to Get Your Rapid-Results Guide</strong></a></p><p>(To download, on the page that opens up when you click the link, hover at the top to see the menu, and click on the symbol that looks like a down arrow on paper.)</p></div>'
        }
        
    },
    fttmr: {
        title: 'Fast Track To Mr Right',
        hasposts: true,
        hascategories: true,
        data: [
            {
                id:'live_with_matt',
                title: 'Live With Matt', 
                posts: [
                    {title: 'Your Next Live Coaching With Matt', video: true, audio: false},
                    {title: 'Past Session Recordings', video: true, audio: false},
                    {title: 'Live Access (day of session)', video: true, audio: false},
                    {title: 'Congrats on Getting on the Fast Track!', video: true, audio: false},
                ]
            }, 
            {
                id:'fast_track1',
                title: 'Fast Track #1', 
                posts: [
                    { title : '1. Discover What You Want in Love (and the Fastest Way to Get it)', video: true, audio: false, videId: '34800811'},
                    { title : '2. Meet Amazing Men Now – Your Quick-Start Action Plan', video: true, audio: false, videId: '34850981'},
                    { title : '3. How to Create Red-Hot Chemistry (and Never Get “Friend-Zoned” Again!)', video: true, audio: false, videId: '34782418'},
                    { title : '4. Confessions of the World’s Biggest “Player”: What Finally Made Me Commit', video: true, audio: false, videId: '225222119'},
                    { title : '5. You Asked, Men Answer #1: A Roundtable Q+A with Real Guys', video: true, audio: false, videId: '226159002'},
                    { title : 'Your Next Live Coaching With Matt', video: true, audio: false, videId: ''},
                ]
            }, 
            {
                id:'fast_track2',
                title: 'Fast Track #2', 
                posts: [
                    { title: '1. The Shocking Reason Men Don’t Approach You (It’s Not at All What You Think)' , video: true, audio: false, videId: '34765628'},
                    { title: '2. The High Value Woman’s Guide to Calling, Texting and Dating' , video: true, audio: false, videId: '34898852'},
                    { title: '3. Stop Wasting Time! Know if He’s Mr. Right or Mr. Wrong in Just One Week' , video: true, audio: false, videId: '34901788'},
                    { title: '4. A Top Male Dating Coach Reveals: The Truth About Men' , video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/al-interview-m2.mp3'},
                    { title: '5. You Asked, Men Answer #2: A Roundtable Q+A with Real Guys' , video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/trainer-call-2.mp3'},
                    { title: 'Your Next Live Coaching With Matt' , video: true, audio: false, videId: ''},
                ]
            },
            {
                id:'fast_track3',
                title: 'Fast Track #3', 
                posts: [
                    {title: '1. How To Transition Into A Relationship', video: true, audio: false, videId: '34921067'},
                    {title: '2. Attracting The Men You’ve Always Wanted', video: true, audio: false, videId: '34776858'},
                    {title: '3. The Keys To Instant Attraction', video: true, audio: false, videId: '34780431'},
                    {title: 'Bonus #2: A Taster Of “Keep The Guy”', video: true, audio: false, videId: '41343460'},
                    {title: 'Interview #3: Jordan Harbinger – Mysteriously Simple Men', video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/3-interview-jordan-harbinger.mp3'},
                    {title: 'Trainer Q&A #3', video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/trainer-call-3.mp3'},
                    {title: 'Your Next Live Coaching With Matt', video: true, audio: false, videId: ''},
                ]
            },
            {
                id:'fast_track4',
                title: 'Fast Track #4', 
                posts: [
                    {title: '1. The Power Of Touch', video: true, audio: false, videId: '34788488'},
                    {title: '2. Working A Room', video: true, audio: false, videId: '34810403'},
                    {title: '3. How To Deal With Social Fear And Rejection', video: true, audio: false, videId: '34814537'},
                    {title: '4. Feedback From The Live Experience With Attendees!', video: true, audio: false, videId: '34819560'},
                    {title: 'Interview #4: Mat Boggs – 6 Questions To Meet Your Dream Guy', video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/4-interview-mat-boggs.mp3'},
                    {title: 'Trainer Q&A #4', video: false, audio: true, audioUrl: 'ttps://dl.howtogettheguy.com/fasttrack/audio/trainer-call-4.mp3'},
                    {title: 'Your Next Live Coaching With Matt', video: true, audio: false, videId: ''},
                ]
            }, 
            {
                id:'fast_track5',
                title: 'Fast Track #5', 
                posts: [
                    {title: '1. Removing The Negative Thoughts That Hold Us Back', video: true, audio: false, videId: '34824857'},
                    {title: '2. Relationship Destroyers', video: true, audio: false, videId: '34924117'},
                    {title: '3. Creating An Extraordinary Sex Life', video: true, audio: false, videId: '34906747'},
                    {title: 'Interview #5: Richard La Ruina – How To Distinguish Yourself From Other Women', video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/5-interview-richard-la-ruina.mp3'},
                    {title: 'Your Next Live Coaching With Matt', video: true, audio: false, videId: ''},
                ]
            },
            {
                id:'fast_track6',
                title: 'Fast Track #6', 
                posts: [
                    {title: '1. Tying It All Together', video: true, audio: false, videId: '52996403'},
                    {title: '2. The Male Masterclass', video: false, audio: false, pdf: 'https://dl.howtogettheguy.com/fasttrack/M6C-The-Male-Masterclass.pdf'},
                    {title: '3. Creating A Beautiful Life', video: false, audio: false, pdf: 'https://dl.howtogettheguy.com/fasttrack/M6B-Creating-A-Beautiful-Life.pdf'},
                    {title: '4. Private Q&A Webinar With Matthew', video: false, audio: true, audioUrl: 'https://dl.howtogettheguy.com/fasttrack/audio/private-webinar-m6.mp3?_ga=2.185704809.1507331879.1550507577-787775824.1516708381'},
                    {title: '5. Bonus: Audio Download', video: true, audio: false, videId: ''},
                    {title: 'Your Next Live Coaching With Matt', video: true, audio: false, videId: ''},
                ]
            }
        ]
    },
    impact: {
        title: 'Impact',
        hasposts: true,
        hascategories: true,
        data: [
            {
                id:'impact_seminar',
                title: 'Impact Seminar', 
                posts: [
                    {title: 'Module #1 – What is Impact?', video: true, audio: false },
                    {title: 'Module #2 – The body language of Impact', video: true, audio: false },
                    {title: 'Module #3 – Projecting Impact through gestures', video: true, audio: false },
                    {title: 'Module #4 – Become a master conversationalist', video: true, audio: false },
                    {title: 'Module #5 – How to be instantly likeable', video: true, audio: false },
                    {title: 'Module #6 – Instant emotional strength', video: true, audio: false },
                    {title: 'Module #7 – Get a VIP social circle', video: true, audio: false },
                    {title: 'Module #8 – The secret to becoming unfazeable', video: true, audio: false },
                    {title: 'Module #9 – How to work any room', video: true, audio: false },
                    {title: 'Module #10 – Sell yourself through storytelling', video: true, audio: false },
                    {title: 'Module #11 – Your burning questions answered', video: true, audio: false },
                ]
            }, 
            {
                id:'impact_bonuses',
                title: 'Impact Bonuses', 
                posts: [
                    {title: 'Bonus #1 – Audio download', video: true, audio: false},
                    {title: 'Bonus #2 – Tying it all together webinar', video: true, audio: false},
                    {title: 'Bonus #3 – How to make friends with important people – Adam Gilad', video: true, audio: false},
                    {title: 'Bonus #4 – How to find incredible mentors who will help you along the way', video: true, audio: false},
                    {title: 'Bonus #5 – ‘Week in a day’ webinar', video: true, audio: false},
                ]
            }, 
            {
                id:'impact_audio',
                title: 'Impact Audio', 
                posts: [
                    {title: 'Full audio download', video: false, audio: true},
                    {title: 'Module #1 – What is Impact?', video: false, audio: true},
                    {title: 'Module #2 – The body language of Impact', video: false, audio: true},
                    {title: 'Module #3 – Projecting Impact through gestures', video: false, audio: true},
                    {title: 'Module #4 – Become a master conversationalist', video: false, audio: true},
                    {title: 'Module #5 – How to be instantly likeable', video: false, audio: true},
                    {title: 'Module #6 – Instant emotional strength', video: false, audio: true},
                    {title: 'Module #7 – Get a VIP social circle', video: false, audio: true},
                    {title: 'Module #8 – The secret to becoming unfazeable', video: false, audio: true},
                    {title: 'Module #9 – How to work any room', video: false, audio: true},
                    {title: 'Module #10 – Sell yourself through storytelling', video: false, audio: true},
                    {title: 'Module #11 – Your burning questions answered', video: false, audio: true},
                ]
            }, 
            {
                id:'master_of_impact_interview_series',
                title: 'Masters of Impact Interview Series', 
                posts: [
                    {title: '1. How To Talk To Anyone – Jon Turtletaub', video: true, audio: false },
                    {title: '2. How to Live a Life of Passion, Positivity and Purpose – Kathy Eldon', video: true, audio: false },
                    {title: '3. The Woman Behind Ryan Seacrest Shares Her Secrets To Success – Claudine Cazian', video: true, audio: false },
                    {title: '4. Become a Celebrity In Your Own World – Daniel Musto', video: true, audio: false },
                    {title: '5. How To Prepare For The Oscars – Louise Roe', video: true, audio: false },
                    {title: '6. Secrets To Getting Ahead In Your Life & Career – Karen Rinaldi', video: true, audio: false },
                    {title: '7. Public Speaking Mastery – Steve Hussey', video: true, audio: false },
                    {title: '8. Powerful Strategies for a Fearless Life – Jesse Ray Vasquez', video: true, audio: false },
                    {title: '9. How To Perform Under Pressure In Interviews, Auditions, And Big Moments In Life – Paul Ruddy', video: true, audio: false },
                    {title: '10. Explode Your Network Overnight – Kevin Winston', video: true, audio: false },
                    {title: '11. Live Life Passionately, Do What You Love – Lewis Howes', video: true, audio: false },
                    {title: '12. How to Get Your Dream Job…In the Smartest Way Possible – Nur', video: true, audio: false },
                ]
            }, 
        ]
    },
    aam:{
        title: 'Attract Any Man',
        hasposts: true,
        hascategories: false,
        data: {
            title: 'Attract Any Man',
            posts: [
                {title: '1. Foundations', video: true, audio: false },
                {title: '2. Getting The Guy', video: true, audio: false },
                {title: '3. Ensuring Success', video: true, audio: false },
                {title: '4. The Male Mind', video: true, audio: false },
                {title: 'Bonus #1: Audio Download', video: true, audio: false },
                {title: 'Bonus #2: The Deadliest Mistakes Women Make In The Bedroom', video: true, audio: false },
                {title: 'Bonus #3: 7 Shockingly Simple Flirting Secrets', video: true, audio: false },
                {title: 'Bonus #4: The Ultimate Cheat Sheet For Attracting a Man Who Lives Up to Your High Standards', video: true, audio: false },
                {title: 'Bonus #5: Permanently Getting Rid of Jealousy', video: true, audio: false },
            ]
        }
    },
    htttm:{
        title: 'How to Talk to Men (Secret Scripts)',
        hasposts: true,
        hascategories: false,
        data: {
            title: 'How to Talk to Men (Secret Scripts)',
            posts: [
                {title: 'Core Program: The 59 Secret Scripts', video: true, audio: false },
                {title: 'Bonus #1: Texting Magic', video: true, audio: false },
                {title: 'Bonus #2: 5 Compliments Guide', video: true, audio: false },
                {title: 'Bonus #3: Is He Worth Dating?', video: true, audio: false },
                {title: 'Bonus #4: Perfect Dating Profile', video: true, audio: false },
                {title: 'Bonus #5: 10 Essential Texts', video: true, audio: false },
                {title: 'Bonus #6: Language of Desire', video: true, audio: false },
                {title: 'Bonus #7: Deep Dive Communication Webinar', video: true, audio: false },
                {title: 'Bonus #8: Matt’s Confidence File', video: true, audio: false }
            ]
        }
        
    },
    cm:{
        title: 'Communication Masterclass',
        hasposts: true,
        hascategories: false,
        data: {
            title: 'Communication Masterclass',
            posts: [
                {title: 'Master Class Video', video: true, audio: false },
                {title: 'Bonus #1: Audio Version', video: true, audio: false },
                {title: 'Bonus #2: Full Transcription', video: true, audio: false },
                {title: 'Bonus #3: Cheat Sheet', video: true, audio: false },
                {title: 'Bonus #4: 4 Types of High Value Women', video: true, audio: false },
                {title: 'Bonus #5: Secrets of the Male Mind', video: true, audio: false },
                {title: 'Bonus #6: Unstoppable Confidence', video: true, audio: false }
            ]
        }
        
    },
    ghrbty:{
        title: 'Get Him Running Back to You',
        hasposts: true,
        hascategories: false,
        data: {
            title: 'Get Him Running Back to You',
            posts: [
                {title: '1. Core Audio Program: The 5 Simple Steps', video: true, audio: false },
                {title: '2. BONUS: Get Him Running Back to You (Ebook)', video: true, audio: false },
                {title: '3. BONUS: Irresistible You with Celebrity Stylist Daniel Musto', video: true, audio: false },
                {title: '4. BONUS: Get Him Running Back to You Roadmap', video: true, audio: false },
                {title: '5. BONUS: Back to Life: How to Recover From a Breakup Fast', video: true, audio: false },
                {title: '6. BONUS: 3 Minute Mind Frames', video: true, audio: false },
                {title: '7. BONUS: What to Text Your Ex', video: true, audio: false },
                {title: '8. BONUS: The 10 Magic Keys to Getting Him Back', video: true, audio: false },
                {title: '9. BONUS: Permanently Getting Rid of Jealousy', video: true, audio: false },
                {title: '10. BONUS: Deep Dive Webinar', video: true, audio: false },
                {title: '11. BONUS: Get Him Back from a Long Distance', video: true, audio: false }
            ]
        }
    },
    ktg: {
        title: 'Keep The Guy',
        hasposts: true,
        hascategories: true,
        data: [
            {
                id:'keep_the_guy',
                title: 'Keep The Guy', 
                posts: [
                    {title: '1. Rewiring Ourselves For Commitment', video: true, audio: false },
                    {title: '2. Q&A #1', video: true, audio: false },
                    {title: '3. How To Tell If He’s Ready To Commit', video: true, audio: false },
                    {title: '4. Everything You Need To Know About First Dates', video: true, audio: false },
                    {title: '5. Exclusivity', video: true, audio: false },
                    {title: '6. Q&A #2', video: true, audio: false },
                    {title: '7. How To Integrate A Guy Into Your Life', video: true, audio: false },
                    {title: '8. How To Deal With ‘Guy Time’', video: true, audio: false },
                    {title: '9. Q&A #3', video: true, audio: false },
                    {title: '10. Dealing With Jealousy', video: true, audio: false },
                    {title: '11. Lasting Commitment', video: true, audio: false },
                    {title: 'Audio Download', video: true, audio: false },
                ]
            }, 
            {
                id:'keep_the_guy_webinar',
                title: 'Keep The Guy Webinar', 
                posts: [
                    {title: 'Webinar #1', video: true, audio: false },
                    {title: 'Webinar #2', video: true, audio: false },
                    {title: 'Webinar #3', video: true, audio: false }
                ]
            }, 
        ]
    },
    fftfb:{
        title: 'From Flirting To Forever Bonuses',
        hasposts: true,
        hascategories: false,
        data: {
            title: 'Coaching Bonuses',
            posts: [
                {title: 'Coaching #1 – Ellie – What Should I Do With My Life?', video: true, audio: false},
                {title: 'Coaching #2 – Gaitee – Getting Back In Balance', video: true, audio: false},
                {title: 'Coaching #3 – Mojgan – Stop Being A People Pleaser!', video: true, audio: false},
                {title: 'Coaching #4 – Heather – Unlocking Adventurousness', video: true, audio: false},
                {title: 'Coaching #5 – Secrets Of The Male Mind Ebook', video: true, audio: false}
            ]
        }
        
    },
}