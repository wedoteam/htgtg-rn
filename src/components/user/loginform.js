import React, { Component } from 'react'
import { Input, InputGroup, Label, Item, Button, Toast } from 'native-base'
import { Text, View, KeyboardAvoidingView } from 'react-native'

import {styles} from '../../styles/login';

export class LoginForm extends Component {

  constructor(props){
    super(props);
    this.state={
      username: '',
      password: ''
    }
    this.navigateForgotPassword = this.navigateForgotPassword.bind(this);
    this.submitLoginForm = this.submitLoginForm.bind(this);
  }

  navigateForgotPassword(){
    this.props.navigation.navigate('ForgotPassword');
  }

  submitLoginForm(){
    this.props.loginUser(this.state);
  }

  render() {
    return (
      <View style={styles.formWrapper}>
        <Item stackedLabel style={styles.formItem}>
          <Label style={styles.formLabel}>Email ID</Label>
          <Input autoCapitalize='none' style={styles.inputItem} value={this.state.username} onChangeText={(text) => {this.setState({username: text})}} />
        </Item>
        <View style={{height:10}} />
        <Item stackedLabel last style={styles.formItem}>
          <Label style={styles.formLabel}>Password</Label>
          <Input secureTextEntry={true} style={styles.inputItem} value={this.state.password} onChangeText={(text) => {this.setState({password: text})}} />
        </Item>
        <View style={{height:16}} />
        <View>
          <Text onPress={this.navigateForgotPassword} style={styles.forgotpasstext}>Forgot Password ?</Text>
        </View>
        <View style={{height:16}} />
        <Button block style={styles.buttonPrimary} onPress={this.submitLoginForm}><Text style={{color:'#fff'}}>Login to my Account</Text></Button>
        <View style={{height:16}} />
      </View>
    )
  }
}

export default LoginForm;