import React, { Component } from 'react'
import { Input, InputGroup, Label, Item, Button } from 'native-base'
import { Text, View } from 'react-native'

import {styles} from '../../styles/login';

export class ForgotPasswordForm extends Component {
  render() {
    return (
      <View style={styles.formWrapper}>
        <Item stackedLabel style={styles.formItem}>
          <Label style={styles.formLabel}>Email ID</Label>
          <Input style={styles.inputItem} />
        </Item>
        <View style={{height:16}} />
        <Button block style={styles.buttonPrimary}><Text style={{color:'#fff'}}>Submit</Text></Button>
        <View style={{height:16}} />
      </View>
    )
  }
}

export default ForgotPasswordForm;