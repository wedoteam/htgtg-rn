import React, { Component } from 'react';
import { View, StatusBar, Platform, TouchableOpacity } from 'react-native';
import { Header, Text, Left, Body, Right, Button } from 'native-base';
import { headerStyles } from '../styles/app';
import Icon from 'react-native-vector-icons/Ionicons'
import LinearGradient from 'react-native-linear-gradient'

export class AppHeader extends Component {

  _headerContent(){

    let menuIcon = 'md-menu';
    let backIcon = 'md-arrow-back';
    let cartIcon = 'md-cart';
    if(Platform.OS == 'ios'){
        backIcon = 'ios-arrow-round-back';
        menuIcon = 'ios-menu';
        cartIcon = 'ios-cart';
    }

    let iconStyle = headerStyles.icons;
    let textStyle = headerStyles.title;
    if(this.props.translucent){
      iconStyle = headerStyles.whiteicons;
      textStyle = headerStyles.whitetitle;
    }

    return(
      <View style={{flex: 1, justifyContent:'space-between', flexDirection: 'row',}}>
        <Left>
            {this.props.showBack ?
            <Button transparent onPress={() => {this.props.navigation.goBack(null);}}>
                <Icon size={24} style={iconStyle} name={backIcon} />
            </Button>
            : 
            <Button transparent onPress={() => {this.props.navigation.openDrawer();}}>
                <Icon size={24} style={iconStyle} name={menuIcon} />
            </Button>
            }
        </Left>
        <Body><Text style={textStyle} >{this.props.title}</Text></Body>
        <Right></Right>
      </View>
    )
  }

  render() {

    if(this.props.translucent){
      return (
        <LinearGradient colors={['#00000085', '#00000060', '#00000000']} style={headerStyles.headerGradient} >
          <Header noShadow style={headerStyles.translucentContainer}  >
            <StatusBar backgroundColor="#000" barStyle="light-content" />
            {this._headerContent()}
          </Header>
        </LinearGradient>
      )
    }

    return (
      <Header noShadow style={headerStyles.container}  >
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        {this._headerContent()}
      </Header>
    )
  }
}

export default AppHeader