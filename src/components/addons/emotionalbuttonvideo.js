import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { emotionalButtonStyles } from '../../styles/app'
import { emotionalButtons } from '../../data';

const vidIcon = require('../../assets/play.png');

class EmotionalButtonVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onPress(video){
    this.props.openVideoModel(video);
  }

  render() {
    return (
      <View>
        {this.state.isLoading &&
            <View style={emotionalButtonStyles.imageLoader}>
                <ActivityIndicator />
        </View> }
        <TouchableOpacity activeOpacity={0.8} onPress={() => {
            this.onPress(this.props.data.content);
        }} >
            <Image source={{uri: this.props.data.thumb}} style={emotionalButtonStyles.image} onLoadEnd={() => {
                this.setState({isLoading: false})
            }} />
        </TouchableOpacity>
        <View style={emotionalButtonStyles.playIconWrap}>
            <TouchableOpacity activeOpacity={0.8} onPress={() => {
                this.onPress(this.props.data.content);
            }}>
            <Image source={vidIcon} style={emotionalButtonStyles.playIcon} />
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default EmotionalButtonVideo;
