import React, { Component } from 'react';
import { View, Image, ActivityIndicator, TouchableOpacity } from 'react-native';
import { emotionalButtonStyles } from '../../styles/app'

class EmotiobnalButtonImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true
    };
  }



  render() {
    return (
      <View>
        {this.state.isLoading &&
            <View style={emotionalButtonStyles.imageLoader}>
                <ActivityIndicator />
        </View> }
        <TouchableOpacity activeOpacity={0.8} onPress={() => {
            this.props.openImageModel(this.props.data.content);
        }} >
        <Image source={{uri: this.props.data.content}} style={emotionalButtonStyles.image} onLoadEnd={() => {
            this.setState({isLoading: false})
        }} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default EmotiobnalButtonImage;
