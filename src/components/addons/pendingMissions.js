import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Text, Grid, Col, Button } from 'native-base'
import { missionsStyles } from '../../styles/app'

const icon = require('../../assets/mission-pending.png');

export class PendingMissions extends Component {
  render() {
    return (
      <View>
        {this.props.data.map(item => {
            return(
                <Grid key={item.id} style={missionsStyles.itemWrap}>
                    <Col style={{width: 26, paddingTop: 16}}><Image source={icon} /></Col>
                    <Col style={missionsStyles.contentWrap}>
                        <View style={{height: 16}} />
                        <Text style={missionsStyles.title}>{item.title}</Text>
                        <View style={{height: 8}} />
                        <Text style={missionsStyles.desc}>{item.disc}</Text>
                        <View style={{height: 16}} />
                        <View style={{flex: 1, alignSelf: 'flex-end' }}>
                            <Button transparent bordered small style={{borderColor:'#333'}} onPress={() => {
                                this.props.openMissionModel();
                            }} ><Text style={{color:'#333'}}>COMPLETE MISSION</Text></Button>
                        </View>
                        <View style={{height: 16}} />
                    </Col>
                </Grid>
            )
        })}
      </View>
    )
  }
}

export default PendingMissions