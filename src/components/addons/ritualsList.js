import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text, Button, Grid, Col } from 'native-base'

class RitualsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.trackRitual = this.trackRitual.bind(this);
    this.deleteRitual = this.deleteRitual.bind(this);
  }

  trackRitual(item){
    console.log(item);
  }

  deleteRitual(item){
    console.log(item);
  }

  render() {
    return (
      <View>
        {this.props.rituals.map(item => {
            return(
                <View style={{backgroundColor:'#E6EDEF',padding: 16, marginBottom: 10}}>
                    <Grid>
                        <Col>
                            <Text style={{color:'#666', fontSize: 16}}>{item.title}</Text>
                            <View style={{flexDirection:'row', paddingTop: 4,}}>
                                {!item.isComplete && 
                                    <TouchableOpacity onPress={() => this.trackRitual(item)}><Text style={{color: '#EC4499', marginRight: 16}}>Track Ritual</Text></TouchableOpacity>
                                }
                                <TouchableOpacity onPress={() => this.deleteRitual(item)}><Text style={{color: '#EC4499'}}>Delete</Text></TouchableOpacity>
                            </View>
                        </Col>
                        <Col style={{width: 28, justifyContent:'center'}}>
                            <View style={{width:28, height: 28, backgroundColor:'#8B96A4', borderRadius: 4}} />
                        </Col>
                    </Grid>
                </View>
            )
        })}
      </View>
    );
  }
}

export default RitualsList;
