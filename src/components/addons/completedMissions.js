import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Text, Grid, Col, Button } from 'native-base'
import { missionsStyles } from '../../styles/app'

const icon = require('../../assets/mission-completed.png');

export class CompletedMissions extends Component {
  render() {
    return (
      <View>
        {this.props.data.map(item => {
            return(
                <Grid key={item.id} style={missionsStyles.itemWrap}>
                    <Col style={{width: 26, paddingTop: 16}}><Image source={icon} /></Col>
                    <Col style={missionsStyles.contentWrap}>
                        <View style={{height: 16}} />
                        <Text style={missionsStyles.title}>{item.title}</Text>
                        <View style={{height: 8}} />
                        <Text style={missionsStyles.desc}>{item.disc}</Text>
                        <View style={{height: 12}} />
                        <Text style={missionsStyles.text}>{item.text}</Text>
                        <View style={{height: 16}} />
                    </Col>
                </Grid>
            )
        })}
      </View>
    )
  }
}

export default CompletedMissions