import React, { Component } from 'react'
import { View, Image, Modal, StatusBar } from 'react-native'
import { Text, Grid, Col, Button, Textarea, Row } from 'native-base'
import { missionsStyles } from '../../styles/app'

const icon = require('../../assets/mission-pending.png');

class CompleteMissionModel extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <Modal  
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => {
            this.props.closeMissionModel();
        }}
      >
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <View style={{flex: 1, backgroundColor:'#00000095', paddingHorizontal: 10}}>
          <View style={{flex: 0.75}} />
          <Grid style={[missionsStyles.itemWrap]}>
            <Col style={{width: 26, paddingTop: 16}}><Image source={icon} /></Col>
            <Col style={missionsStyles.contentWrap}>
                <View style={{height: 16}} />
                <Text style={missionsStyles.title}>Complete Mission</Text> 
                <View style={{height: 8}} />
                <Text style={missionsStyles.title}>MISSION 04</Text>
                <View style={{height: 8}} />
                <Text style={missionsStyles.desc}>Say “hi” to 2 new people this week and ask how their day is going or deliver a compliment.</Text>
                <View style={{height: 8}} />
                <Textarea  bordered placeholder='Add your comment...' />
                <View style={{height: 16}} />
                <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', }}>
                  <Button bordered small style={{borderColor:'#333', marginRight: 10, backgroundColor:'#333'}} onPress={() => {
                      this.props.closeMissionModel();
                    }} >
                      <Text style={{color:'#fff'}}>CANCEL</Text>
                  </Button>
                  <Button transparent bordered small style={{borderColor:'#333'}} onPress={() => {
                      this.props.closeMissionModel();
                  }} >
                    <Text style={{color:'#333'}}>COMPLETE MISSION</Text>
                  </Button>
                  
                </View>

            </Col>
          </Grid>
          <View style={{flex: 0.75}} />
        </View>
      </Modal>
    );
  }
}

export default CompleteMissionModel;
