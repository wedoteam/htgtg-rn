import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { emotionalButtonStyles } from '../../styles/app'

class EmotionalButtonText extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={emotionalButtonStyles.textWrap}>
        <Text>{this.props.data.content}</Text>
      </View>
    );
  }
}

export default EmotionalButtonText;
