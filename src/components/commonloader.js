import React, { Component } from "react";
import { View, Text, Modal, ActivityIndicator } from "react-native";
import Proptypes from "prop-types";

const CommonLoader = props => {
  if (props.loading) {
    return (
      <View
        style={{
          position: "absolute",
          backgroundColor: "#000000",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }}
      >
        <Modal
          animationType="slide"
          transparent
          visible
          onRequestClose={() => {}}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#ffffff",
                width: 80,
                height: 80,
                borderRadius: 40,
                shadowColor: "#000000",
                shadowOffset: {
                  width: 0,
                  height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.1,
                elevation: 10
              }}
            >
              <ActivityIndicator size="small" color="#101010" />
            </View>
          </View>
        </Modal>
      </View>
    );
  } else {
    return null;
  }
};

CommonLoader.propTypes = {
  text: Proptypes.string,
  loading: Proptypes.bool
};

export default CommonLoader;
