import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Text, Grid, Col } from 'native-base';
import PropTypes from 'prop-types'

import {moduleStyles} from '../../styles/app';

const icon = require('../../assets/athr-module-catlist.png');

class ATHRCategoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Grid style={moduleStyles.ATHRCategoryItemWrap}>
        <Col>
            <Text style={moduleStyles.athrciTitle} >{this.props.title}</Text>
            <Text style={moduleStyles.athrciSubTitle}>{this.props.subtitle}</Text>
        </Col>
        <Col style={moduleStyles.athrciIconWrap}>
            <Image source={icon} style={moduleStyles.athrciIcon} /> 
        </Col>
      </Grid>
    );
  }
}

ATHRCategoryItem.PropTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string
}

export default ATHRCategoryItem;
