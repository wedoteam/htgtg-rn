import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { journalData, activityWorksheetData } from '../../data';

import SegmentControl from 'react-native-segment-controller';

import ArticleComments from '../common/articleComment'
import JournalList from '../common/journalList'
import WorkSheetList from '../common/workSheetList'

export class ArticleContent extends Component {
  
    constructor(props) {
      super(props);
      this.state = {
        activeTab: 0
      };
    }
  

  render() {
    console.log(this.props);
    return (
      <View style={{flex: 1}}>
        {this.props.showJournal &&
          <View style={{flex: 1 }}>
            <View style={{height: 8}} />
            <SegmentControl
              values={['Journal','Activity Worksheet']}
              selectedIndex={this.state.activeTab}
              height={30}
              onTabPress={(item) => {
                this.setState({activeTab: item})
              }}
              borderRadius={5}
              activeTabStyle={{backgroundColor:'#333'}}
              tabTextStyle={{color:'#333'}}
              tabStyle={{borderColor:'#333'}}
              tabsContainerStyle={{paddingHorizontal: 16}}
            />
            <View style={{height: 8}} />
            {this.state.activeTab == 0 ? 
              <JournalList data={journalData} /> : 
              <WorkSheetList data={activityWorksheetData} />
            }
          </View>
        }
        
        {!this.props.showComments && this.props.desc != '' && 
          <View style={{paddingHorizontal: 16}}>
          <View style={{height: 16}} />
          <Text style={{fontSize: 15, color: '#333333'}}>{this.props.desc}</Text>
          </View>
        }

        {this.props.showComments &&
          <ArticleComments desc={this.props.desc} />
        }
      </View>
    )
  }
}

export default ArticleContent