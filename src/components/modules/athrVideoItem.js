import React, { Component } from 'react'
import { View, Image, ImageBackground } from 'react-native'
import {Text, Grid, Row} from 'native-base';
import LinearGradient from 'react-native-linear-gradient'

import {moduleStyles} from '../../styles/app'

export default class ATHRVideoItem extends Component {

  componentDidMount(){

  }

  render() {
    const {title, module, img} = this.props.item;
    
    return (
      <View style={{ paddingLeft: 16 }}>
        <ImageBackground source={{uri: img}} style={moduleStyles.athVideoListItemBG} imageStyle={moduleStyles.athVideoListItemBGImage}>
          <Grid>
            <Row style={moduleStyles.moduleModTextWrap}>
              <LinearGradient colors={['#00000085', '#00000000']} style={moduleStyles.moduleModTextGradient} >
                <Text style={moduleStyles.moduleModText} >{module}</Text>
              </LinearGradient>
            </Row>
            <Row style={moduleStyles.moduleTitleTextWrap}>
              <LinearGradient colors={['#00000000', '#00000085']} style={moduleStyles.moduleTitleTextGradient} >
                <Text style={moduleStyles.moduleTitleText}>{title}</Text>
              </LinearGradient>
            </Row>
          </Grid>
        </ImageBackground>
      </View>
    )
  }
}