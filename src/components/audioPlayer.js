import React from 'react'
import { View, Image, Text, Slider, TouchableOpacity, Platform, Alert, ActivityIndicator, StyleSheet} from 'react-native';
import Sound from 'react-native-sound'

import Video from 'react-native-video'

const img_pause = require('../assets/icon-5.png');
const img_play = require('../assets/icon-7.png');
const img_playjumpleft = require('../assets/icon-9.png');
const img_playjumpright = require('../assets/icon-8.png');

export default class AudioPlayeriOS extends React.Component{

    static navigationOptions = props => ({
        title:props.navigation.state.params.title,
    })

    constructor(){
        super();
        this.state = {
            paused: false,
            totalLength: 1,
            currentPosition: 0,
            selectedTrack: 0,
            isLoading: true
        }
        this.sliderEditing = false;
    }

    setDuration(data) {
        console.log('setDuration');
        console.log(data.duration);
        this.setState({totalLength: Math.floor(data.duration)});
    }

    setTime(data) {
        if(!this.sliderEditing)
        this.setState({
            currentPosition: Math.floor(data.currentTime),
            isLoading: false
        });
    }

    seek(time) {
        // time = Math.round(time);
        this.refs.audioElement && this.refs.audioElement.seek(time);
        this.setState({
            currentPosition: time,
            paused: false,
        });
    }

    onSliderEditStart = () => {
        this.sliderEditing = true;
    }
    onSliderEditEnd = () => {
        this.sliderEditing = false;
    }
    onSliderEditing = value => {
        this.seek(value);
    }

    videoError(error){
    
    }

    loadStart(data){
        console.log('LOAD START');
    }

    onEnd(){
        console.log('LOAD ENDED');
    }

    play(){
        this.setState({paused: false})
    }

    onBuffer(data){
        console.log('BUFFERING');
        console.log(data);
        this.setState({
            isLoading: data.isBuffering
        })
    }

    pause(){
        this.setState({paused: true})
    }

    getAudioTimeString(seconds){
        const h = parseInt(seconds/(60*60));
        const m = parseInt(seconds%(60*60)/60);
        const s = parseInt(seconds%60);

        return ((h<10?'0'+h:h) + ':' + (m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
    }

    jumpPrev15Seconds(){
        let seconds = (this.state.currentPosition);
        let seekTime = seconds - 10;
        this.seek(seekTime);
    }

    jumpNext15Seconds(){
        let seconds = (this.state.currentPosition);
        let seekTime = seconds + 10;
        this.seek(seekTime);
    }

    render(){ 

        const currentTimeString = this.getAudioTimeString(this.state.currentPosition);
        const durationString = this.getAudioTimeString(this.state.totalLength);
        
        return (
            <View style={{justifyContent:'center', borderBottomColor:'#ddd', borderBottomWidth: 1 }}>
                <View style={{height: 16}} />
                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', marginVertical:0}}>
                    <TouchableOpacity onPress={this.jumpPrev15Seconds.bind(this)} style={{justifyContent:'center'}}>
                        <Image source={img_playjumpleft} style={{width:35, height:35}}/>
                    </TouchableOpacity>
                    {!this.state.isLoading && !this.state.paused && 
                    <TouchableOpacity onPress={this.pause.bind(this)} style={{marginHorizontal:20}}>
                        <Image source={img_pause} style={{width:50, height:50}}/>
                    </TouchableOpacity>}
                    {!this.state.isLoading && this.state.paused && 
                    <TouchableOpacity onPress={this.play.bind(this)} style={{marginHorizontal:20}}>
                        <Image source={img_play} style={{width:50, height:50}}/>
                    </TouchableOpacity>}

                    {this.state.isLoading && 
                    <View style={styles.audioLoader}>
                        <ActivityIndicator color='#ffffff'/>
                    </View>
                    }

                    <TouchableOpacity onPress={this.jumpNext15Seconds.bind(this)} style={{justifyContent:'center'}}>
                        <Image source={img_playjumpright} style={{width:35, height:35}}/>
                    </TouchableOpacity>
                </View>
                
                <View style={{height: 8}} />
                <View style={{marginVertical:0, marginHorizontal:16, flexDirection:'row'}}>
                    <Text style={{color:'#000', alignSelf:'center'}}>{currentTimeString}</Text>
                    <Slider	
                        onTouchStart={this.onSliderEditStart}
                        // onTouchMove={() => console.log('onTouchMove')}
                        onTouchEnd={this.onSliderEditEnd}
                        // onTouchEndCapture={() => console.log('onTouchEndCapture')}
                        // onTouchCancel={() => console.log('onTouchCancel')}
                        onValueChange={this.onSliderEditing}
                        value={this.state.currentPosition} maximumValue={this.state.totalLength} maximumTrackTintColor='gray' minimumTrackTintColor='#333' thumbTintColor='#333' 
                        style={{flex:1, alignSelf:'center', marginHorizontal:Platform.select({ios:5})}}/>
                    <Text style={{color:'#000', alignSelf:'center'}}>{durationString}</Text>
                </View>
                
                <Video 
                    source={{uri: this.props.audioBook}} 
                    ref="audioElement"
                    onBuffer={this.onBuffer.bind(this)}
                    paused={this.state.paused}
                    resizeMode="cover"
                    repeat={false}
                    onLoadStart={this.loadStart}
                    onLoad={this.setDuration.bind(this)}
                    onProgress={this.setTime.bind(this)}
                    onEnd={this.onEnd}
                    onError={this.videoError}
                    style={{ width: 0, height: 0 }}
                />

                <View style={{height: 16}} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    audioLoader:{
        alignItems:'center',
        justifyContent:'center',
        width: 50, 
        height: 50, 
        borderRadius: 25,
        backgroundColor: '#333333',
        marginHorizontal: 20
    },
});