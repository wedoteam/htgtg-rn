import React, { Component } from 'react';
import { View, Image, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';
import { Text, Grid, Col, Row, List, ListItem, Left, Body, Right } from 'native-base';
import {controlPanelStyles} from '../styles/app';

import {bindActionCreators} from 'redux';
import * as authActions from '../actions/auth'
import {connect} from 'react-redux';

const logoDark = require('../assets/dark_logo.jpg');

export class ControlPanel extends Component {

  constructor(){
    super();

    this.menus = [
        {title: 'My Programs', route:'Home'},
        {title: 'Rituals', route:'Rituals'},
        {title: 'Gratitude Sessions', route:'GratitudeSessions'},
        {title: 'Gratitude Diary', route:'GratitudeDiary'},
        {title: 'Mission', route:'Missions'},
        {title: 'Emotional Buttons', route:'EmotionalButtons'},
        {title: 'Journals', route:'Journals'},
        {title: 'Morning Visualization', route:'MorningVisualization'},
        {title: 'ALTITUDE SESSION', route:'AltitudeSessions'},
        {title: 'Help', route:'Help'},
        {title: 'Logout', route:'logout'}
    ];
    this.handlePress = this.handlePress.bind(this);
  }

  handlePress(item){
    // console.log(item);
    if(item.route == 'logout'){
        this.props.authactions.logoutUser();
    } else {
        if(item.route != ''){
            this.props.navigation.navigate(item.route);
        }
    }
  }

  componentWillReceiveProps(newprops){
      if(!newprops.auth.isAuth){
          this.props.navigation.navigate('Login');
      }
  }

  render() {
    return (
        <View style={{flex: 1}}>
            <SafeAreaView />
            <Grid>
                <Row style={controlPanelStyles.profileWrapper}>
                    <Grid>
                        <Col style={{width: 74}}>
                            <TouchableOpacity activeOpacity={0.75} onPress={() => {
                                this.props.navigation.navigate('MyAccount');
                            }}>
                                <Image source={logoDark} style={controlPanelStyles.appIcon} />
                            </TouchableOpacity>
                        </Col>
                        <Col style={{justifyContent: 'center',}}>
                            <TouchableOpacity activeOpacity={0.75} onPress={() => {
                                this.props.navigation.navigate('MyAccount');
                            }}>
                                <Text style={controlPanelStyles.userName} >{this.props.auth.user.name}</Text>
                                <Text style={controlPanelStyles.userEmail}>{this.props.auth.user.email}</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                </Row>
                
                <Row style={controlPanelStyles.menuWrapper}>
                    <FlatList
                        data={this.menus}
                        numColumns={1}
                        renderItem={({item, index}) => {
                            return(
                                <ListItem button key={index} style={controlPanelStyles.listItem} onPress={() => {this.handlePress(item)}} >
                                    <Left><Text style={controlPanelStyles.linkTitle}>{item.title}</Text></Left>
                                </ListItem>
                            )
                        }}
                    />
                </Row>
            </Grid>
        </View>
    )
  }
}

export default connect(
    state => ({
        auth: state.auth
    }),
    dispatch => ({
        authactions: bindActionCreators(authActions, dispatch)
    })
)(ControlPanel);