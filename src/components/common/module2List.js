import React, { Component } from 'react'
import { View, FlatList, TouchableOpacity } from 'react-native'
import { Text } from 'native-base'

import {module2Styles} from '../../styles/app'

class Module2List extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this._handleOnPress = this._handleOnPress.bind(this);
  }

  _handleOnPress(item, moduleTitle){
    this.props.navigateToDetailsScreen(item, moduleTitle)
  }

  render() {
    return (
      <FlatList
        data={this.props.data}
        numColumns={1}
        renderItem={(item) => {
            return(
                <TouchableOpacity key={item.item.id} onPress={() => this._handleOnPress(item, this.props.moduleTitle)} activeOpacity={0.6}>
                    <View style={module2Styles.listItemWrap}  >
                        <Text style={module2Styles.listTitle}>{item.item.title}</Text>
                        <Text style={module2Styles.listDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</Text>
                    </View>
                </TouchableOpacity>
            )
        }}
        style={module2Styles.flatList}
      />
    );
  }
}

export default Module2List;
