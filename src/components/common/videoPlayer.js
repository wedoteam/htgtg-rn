import React, { Component } from 'react';
import { View, Modal, TouchableOpacity, ActivityIndicator, StyleSheet, Image, SafeAreaView, WebView, StatusBar } from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';
import Video from 'react-native-af-video-player'

import ArticleContent from '../modules/articleContent'

class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
        imageLoading: true
    };
  }

  static propTypes = {
    closeVideoModel: PropTypes.func,
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }

  render() {
    console.log(this.props.video);
    return (
      <Modal
          animationType="slide"
          style={{flex: 1}}
          transparent={true}
          visible={this.props.visible}
          onRequestClose={() => {
              this.props.closeVideoModel();
          }}
      >
        <View style={{ flex:1, padding: 0, backgroundColor: '#fff' }}>
          <SafeAreaView/>
          <Video 
            url={this.props.video}
            autoPlay={true}
            title={this.props.title}
            rotateToFullScreen={true}
            playInBackground={true}
            playWhenInactive={true}
          />

          {/* <View style={{height: 8}} />
          <View style={{paddingHorizontal: 16, borderBottomColor:'#ddd', borderBottomWidth: 1}}>
            <Text style-={{color:'#333', fontSize: 18}} >{this.props.moduleTitle}</Text>
            <View style={{height: 8}} />
            <Text style-={{color:'#333', fontSize: 24, fontWeight: 'bold'}} >{this.props.title}</Text>
            <View style={{height: 8}} />
          </View>
          <View style={{height: 8}} /> */}

          <ArticleContent showComments={this.props.showComments} showJournal={this.props.showJournal} desc={this.props.desc} />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
    loadingView: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,.5)'
    },
    loadingImage: {
      width: 60,
      height: 60,
    }
  });

export default VideoPlayer;