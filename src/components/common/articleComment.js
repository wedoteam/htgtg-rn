import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import { Text, Grid, Col, Footer } from 'native-base'

import { module2Comments } from '../../data';

export class ArticleComments extends Component {
  render() {
    return (
      <View>
        <FlatList
            data={module2Comments}
            numColumns={1}
            ListHeaderComponent={() =>{
                return (
                    <View style={{paddingHorizontal: 16}}>
                        <View style={{height: 8}} />
                        <Text style={{fontSize: 15, color: '#333333'}}>{this.props.desc}</Text>
                        <View style={{height: 16}} />
                        <Text style={{fontSize: 12, color: '#4A4A4A'}}>{module2Comments.length} Comments</Text>
                        <View style={{height: 16}} />
                    </View>
                )
            }}
            ListFooterComponent={() => {
              return(
                <View style={{height: 25}} />
              )
            }}
            renderItem={(item) => {
                const { comment, time, user } = item.item;
                return (
                    <View style={{borderBottomColor: '#ddd', borderBottomWidth: 0, paddingHorizontal: 16}}>
                        <View style={{height: 8}} />
                        <Grid>
                            <Col>
                                <Text style={{fontSize: 14, color: '#333'}}>{user}</Text>
                            </Col>
                            <Col>
                                <Text style={{fontSize: 12, color: '#999999', marginBottom: 8, textAlign: 'right'}}>{time}</Text>
                            </Col>
                        </Grid>
                        
                        
                        <Text style={{fontSize: 14, color: '#666666'}}>{comment}</Text>
                        <View style={{height: 8}} />
                    </View>
                )
            }}
        />
      </View>
    )
  }
}

export default ArticleComments