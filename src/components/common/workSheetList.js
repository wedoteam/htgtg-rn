import React, { Component } from 'react'
import { View, FlatList } from 'react-native'

import { Text } from 'native-base'

export class WorkSheetList extends Component {
  render() {
    return (
      <View>
        <FlatList
            data={this.props.data}
            numColumns={1}
            ListFooterComponent={() => {
                return(
                  <View style={{height: 75}} />
                )
              }}
            renderItem={(item) => {
                const { title, comment } = item.item;
                return (
                    <View style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingHorizontal: 16}}>
                        <View style={{height: 12}} />
                        <Text style={{fontSize: 16, color: '#666666', marginBottom: 8}}>{title}</Text>
                        <Text style={{fontSize: 14, color: '#999999'}}>{comment}</Text>
                        <View style={{height: 12}} />
                    </View>
                )
            }}
        />
      </View>
    )
  }
}

export default WorkSheetList