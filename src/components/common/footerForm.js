import React, { Component } from 'react'
import { View, KeyboardAvoidingView, Platform } from 'react-native'
import { Footer, Text, Grid, Col, Item, Label, Input, Button } from 'native-base'

import Icon from 'react-native-vector-icons/Ionicons'

export class FooterForm extends Component {

  footerRender(){
    return(
      <Footer style={{backgroundColor:'#fff', elevation: 6}}>
        <Grid style={{paddingLeft: 8}}>
            <Col>
                <Input placeholder={this.props.placeholder} />
            </Col>
            <Col style={{ width: 100, alignItems:'flex-end', justifyContent:'center'}}>
                <Button small style={{backgroundColor:'#333'}}><Text>Submit</Text></Button>
            </Col>
        </Grid>
      </Footer>
    )
  }

  render() {
    if(Platform.OS == 'ios'){
      return (
        <KeyboardAvoidingView behavior='padding'>
          {this.footerRender()}
        </KeyboardAvoidingView>
      )
    }

    return (
      this.footerRender()
    )

  }
}

export default FooterForm