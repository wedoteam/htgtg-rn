import React, { Component } from 'react';
import { View, Modal, TouchableOpacity, ActivityIndicator, StyleSheet, Image, SafeAreaView, WebView, StatusBar } from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';

class VideoModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
        imageLoading: true
    };
  }

  static propTypes = {
    closeVideoModel: PropTypes.func,
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }

  render() {
    console.log(this.props.video);

    let video = 'https://player.vimeo.com/video/'+this.props.video;

    console.log(video);

    return (
      <View>
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.props.visible}
            onRequestClose={() => {
                this.props.closeVideoModel();
            }}
        >
                
            <View style={{ flex:1, justifyContent: 'center', padding: 10, backgroundColor: 'rgba(0, 0, 0, 0.9)' }}>
            
            <SafeAreaView style={{position:'absolute', top:20, right:20, zIndex:100}}>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={{ alignItems:'flex-end'}}>
                <TouchableOpacity onPress={() => this.props.closeVideoModel()} >
                    <Text style={{color:'#ffffff'}}>CLOSE</Text>
                </TouchableOpacity>
                </View>
            </SafeAreaView>
            <View style={{flex:0.5, backgroundColor:'transparent'}} />
            <WebView
                style={{flex:1, backgroundColor:'transparent'}}
                javaScriptEnabled={true}
                onLoadEnd={this._onLoadEnd}
                source={{uri: video}}
            />
            {
              this.state.imageLoading &&
              <View style={styles.loadingView}>
                  <ActivityIndicator size='large' color='#ffffff' />
              </View>
            }
            <View style={{flex:0.5, backgroundColor:'transparent'}} />

            </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    loadingView: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,.5)'
    },
    loadingImage: {
      width: 60,
      height: 60,
    }
  });

export default VideoModel;