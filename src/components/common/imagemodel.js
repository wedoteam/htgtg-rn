import React, { Component } from 'react';
import { View, Modal, TouchableOpacity, ActivityIndicator, StyleSheet, Image, SafeAreaView, WebView, StatusBar } from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';

class ImageModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
        imageLoading: true
    };
  }

  static propTypes = {
    closeImageModel: PropTypes.func,
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }

  render() {
    return (
      <View>
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.props.visible}
            onRequestClose={() => {
                this.props.closeImageModel();
            }}
        >
                
            <View style={{ flex:1, justifyContent: 'center', padding: 10, backgroundColor: 'rgba(0, 0, 0, 0.9)' }}>
            
            <SafeAreaView style={{position:'absolute', top:20, right:20, zIndex:100}}>
            <StatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={{ alignItems:'flex-end'}}>
                <TouchableOpacity onPress={() => this.props.closeImageModel()} >
                    <Text style={{color:'#ffffff'}}>CLOSE</Text>
                </TouchableOpacity>
                </View>
            </SafeAreaView>

            <View style={{ flex:1 }}>
                <Image resizeMethod='auto' resizeMode='contain' style={{ flex:1, resizeMethod: 'scaled' }} source={{ uri: this.props.image }}   onLoadStart={() => this._onLoad()} onLoadEnd={() => this._onLoadEnd()} />
                {
                this.state.imageLoading &&
                <View style={styles.loadingView}>
                    <ActivityIndicator size='large' color='#ffffff' />
                </View>
                }
            </View>

            </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    loadingView: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,.5)'
    },
    loadingImage: {
      width: 60,
      height: 60,
    }
  });

export default ImageModel;