import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import { Text } from 'native-base'

export class JournalList extends Component {
  render() {
    return (
      <View>
        <FlatList
            data={this.props.data}
            numColumns={1}
            ListFooterComponent={() => {
              return(
                <View style={{height: 16}} />
              )
            }}
            renderItem={(item) => {
                const { comment, time } = item.item;
                return (
                    <View style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingHorizontal: 16}}>
                        <View style={{height: 10}} />
                        <Text style={{fontSize: 12, color: '#999999', marginBottom: 8, textAlign: 'right'}}>{time}</Text>
                        <Text style={{fontSize: 14, color: '#666666'}}>{comment}</Text>
                        <View style={{height: 10}} />
                    </View>
                )
            }}
        />
      </View>
    )
  }
}

export default JournalList