import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import {Grid, Col, Text} from 'native-base'
import LinearGradient from 'react-native-linear-gradient'

import { videoDetailStyles } from '../../styles/app'

const bg = require('../../assets/video_bg.png');
const playButton = require('../../assets/play.png');


class ArticleTitleView extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <LinearGradient colors={['#00000000', '#00000075']} style={videoDetailStyles.bgGradient} >
        <Grid style={videoDetailStyles.videoDetailsWrap}>
          {!this.props.isAudio &&
            <Col style={videoDetailStyles.videoDetailsImageCol} >
              <View style={videoDetailStyles.videoThumbAbsView}>
                <TouchableOpacity onPress={() => {this.props.onPress()}} activeOpacity={0.9}>
                  <Image source={{uri: this.props.image}} style={videoDetailStyles.videoThumb} />
                  {this.props.isVideo &&
                    <Image source={playButton} style={videoDetailStyles.playButton} />
                  }
                </TouchableOpacity>
              </View>
            </Col>
          }
          <Col style={videoDetailStyles.videoDetailsTitleCol}>
            <Text style={videoDetailStyles.videoTitle} >{this.props.title}</Text>
          </Col>
        </Grid>
        
      </LinearGradient>
    );
  }
}

export default ArticleTitleView;
