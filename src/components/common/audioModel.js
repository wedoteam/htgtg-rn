import React, { Component } from 'react';
import { View, TouchableOpacity, ActivityIndicator, StyleSheet, Image, SafeAreaView, WebView, StatusBar } from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';
import Modal from 'react-native-modalbox';

class AudioModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
        imageLoading: true
    };
  }

  static propTypes = {
    closeAudioModel: PropTypes.func,
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }

  render() {
    console.log(this.props.audio);

    var BContent = <Button onPress={() => this.setState({isOpen: false})} transparent>X</Button>;

    return (
      <View>

        <Modal isOpen={this.props.visible} onClosed={() => this.props.closeAudioModel()} style={[styles.modal, styles.modal4]} position={"center"} backdropContent={BContent}>
                
            <View style={{ flex:1, justifyContent: 'center', padding: 10, backgroundColor: 'rgba(0, 0, 0, 0.9)' }}>
            
            <SafeAreaView style={{position:'absolute', top:20, right:20, zIndex:100}}>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={{ alignItems:'flex-end'}}>
                <TouchableOpacity onPress={() => this.props.closeAudioModel()} >
                    <Text style={{color:'#ffffff'}}>CLOSE</Text>
                </TouchableOpacity>
                </View>
            </SafeAreaView>
            <View style={{flex:0.5, backgroundColor:'transparent'}} />
            <Text>AUDIO MODEL</Text>
            <View style={{flex:0.5, backgroundColor:'transparent'}} />

            </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    loadingView: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,.5)'
    },
    loadingImage: {
      width: 60,
      height: 60,
    }
  });

export default AudioModel;