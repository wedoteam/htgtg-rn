import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';

import ControlPanel from '../components/controlPanel'

import Login from '../containers/login'
import ForgotPassword from '../containers/forgotpassword'
import Home from '../containers/home'
import Category from '../containers/categories'
import DetailView from '../containers/detailview'
import Module from '../containers/module'
import Module2 from '../containers/module2'

import Rituals from '../containers/addons/rituals'
import GratitudeSessions from '../containers/addons/gratitudeSessions'
import GratitudeDiary from '../containers/addons/gratitudeDiary'
import Missions from '../containers/addons/missions'
import EmotionalButtons from '../containers/addons/emotionalButtons'
import Journals from '../containers/addons/journals'
import MorningVisualization from '../containers/addons/morningVisualization'
import AltitudeSessions from '../containers/addons/altitudeSessions'
import Help from '../containers/addons/help'
import MyAccount from '../containers/myaccount'

import LiveCoaching from '../containers/live/liveCoaching'
import LiveSessions from '../containers/live/liveSessions'
import PastSessions from '../containers/live/pastSessions'

const liveStack = createStackNavigator({
    LiveCoaching:{
        screen: LiveCoaching,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LiveSessions:{
        screen: LiveSessions,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    PastSessions:{
        screen: PastSessions,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }
});

const LoginStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }
});

const moduleStack = createStackNavigator({
    Module: {
        screen: Module,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    Category: {
        screen: Category,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    DetailView: {
        screen: DetailView,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
});

const module2Stack = createStackNavigator({
    Module2: {
        screen: Module2,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    DetailView: {
        screen: DetailView,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }
});

const ritualsStack = createStackNavigator({
    Rituals: {
        screen: Rituals,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }
});

const altitudeStack = createStackNavigator({
    AltitudeSessions: {
        screen: AltitudeSessions,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    DetailView: {
        screen: DetailView,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }
})

const gratitudeSessionStack = createStackNavigator({
    GratitudeSessions: {
        screen: GratitudeSessions,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    DetailView: {
        screen: DetailView,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }
})

const MainStack = createDrawerNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    moduleStack: {
        screen: moduleStack,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    module2Stack: {
        screen: module2Stack,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    ritualsStack: {
        screen: ritualsStack
    },
    gratitudeSessionStack:{
        screen: gratitudeSessionStack
    },
    GratitudeDiary: {
        screen: GratitudeDiary,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }, 
    Missions: {
        screen: Missions,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }, 
    EmotionalButtons: {
        screen: EmotionalButtons,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    }, 
    Journals:{
        screen: Journals,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }, 
    MorningVisualization: {
        screen: MorningVisualization,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }, 
    altitudeStack:{
        screen: altitudeStack,
    },
    MyAccount: {
        screen: MyAccount,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }, 
    Help:{
        screen: Help,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    liveStack:{
        screen: liveStack
    }
},{
    contentComponent: ControlPanel
});

export const MainContainer = createAppContainer(MainStack);
export const LoginContainer = createAppContainer(LoginStack);