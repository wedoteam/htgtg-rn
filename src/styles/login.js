import { StyleSheet, Dimensions } from 'react-native';

let { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
    textheading:{
        fontSize:22,
        color:'#fff'
    },
    textsubheading:{
        fontSize:14,
        color:'#fff'
    },  
    container: {
        flex: 1
    },
    loginbg:{
        flex:1,
        width: width,
        backgroundColor:'#000'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    }, 
    formContainer:{
        padding: 16,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    formWrapper:{
        flex:1,
    },
    inputItem:{
        backgroundColor:'#fff',
        flex:1,
        borderRadius: 4,
        borderBottomWidth: 0,
        padding: 10,
        width:(width-32),
        paddingLeft:10
    },
    formItem:{
        borderBottomWidth:0,
        height: 75,
        width:(width-32),
    },
    formLabel:{
        color:'#8B959A',
        marginBottom: 4,
    },
    forgotpasstext:{
        color:'#fff',
        textAlign:'right',
    },
    buttonPrimary:{
        backgroundColor:'#0857AB',
        borderRadius:4
    },
    logoForgotContainer:{
        padding:32,
        flexDirection: 'column',
    },
    backIcon:{
        width:24,
        height:24
    }
})