import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export const liveSessionsStyles = StyleSheet.create({
    text: {
        fontSize: 16,
        color: '#333',
        textAlign: 'center'
    },
    textDate:{
        fontSize: 18,
        color: '#ff0000',
        textAlign: 'center',
        fontWeight: '600'
    },
    pastSessionListItem:{
        paddingHorizontal: 16,
        borderBottomColor:'#ddd',
        borderBottomWidth: 1
    },
    dwBtn:{
        width: 132,
        height: 34
    },
    dwBtnWrap:{
        width: 132,
        justifyContent:'center'
    },
    webviewVideoWrap:{
        width: width,
        height: (9 * width)/16,
    },
    webviewVideo:{
        width: width,
        height: (9 * width)/16,
    },
    webviewChat:{
        flex: 1
    }
});

export const headerStyles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        borderBottomWidth:0
    },
    translucentContainer:{
        borderBottomWidth:0,
        backgroundColor:'#00000000'
    },
    headerGradient:{
        flex: 1
    },
    title:{
        color:'#4A4A4A',
        width:(width-128),
        textAlign:'center'
    },
    icons:{
        color:'#4A4A4A'
    },
    whitetitle:{
        color:'#fff',
        width:(width-128),
        textAlign:'center'
    },
    whiteicons:{
        color:'#fff'
    }
});

export const videoDetailStyles = StyleSheet.create({
    playButton:{
        width: 42,
        height: 42,
        position:'absolute',
        top: 69,
        left: 39
    },
    headerBg:{
        width: width,
        height: 260,
        overflow: 'visible'
    },
    headerBgMaster:{
        width: width,
        height: 310,
        backgroundColor:'#fff'
    },
    headerBgMasterAudio:{
        width: width,
        height: 260,
        backgroundColor:'#fff'
    },
    videoDetailsWrap:{
        alignItems: 'flex-end',
        padding: 16,
        overflow: 'visible'
    },
    videoThumb:{
        width: 120,
        height: 180, 
        borderRadius: 4,
    },
    videoThumbAbsView:{
        position:'absolute',
        top: -120,
        justifyContent:'center',
        alignItems:'center'
    },
    videoTitle:{
        fontSize: 18,
        color:'#fff'
    },
    videoDetailsImageCol:{
        width: 120,
        overflow: 'visible'
    },
    videoDetailsTitleCol:{
        paddingLeft: 8
    },
    bgGradient:{
        flex: 1,
        overflow: 'visible'
    }
});

export const module2Styles = StyleSheet.create({
    tabContainer: {
        backgroundColor:'#ffffff'
    },
    tabHeading:{
        backgroundColor:'#ffffff',
    },
    tabTitle:{
        color: '#666666',
        fontSize: 16,
        padding: 16
    },
    flatList:{
        flex: 1
    }, 
    listTitle:{
        fontSize: 16,
        color: '#333333',
        marginBottom: 8
    },
    listDesc:{
        fontSize: 14,
        color: '#666666'
    },
    listItemWrap: {
        padding: 16,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    }
});

export const moduleStyles = StyleSheet.create({
    moduleModTextWrap:{
        justifyContent: 'flex-start',
    },
    moduleModTextGradient:{
        flex: 1,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: 4
    },
    moduleTitleTextGradient:{
        flex: 1,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        padding: 4
    },
    moduleTitleTextWrap:{
        justifyContent: 'center',
        alignItems:'flex-end',
    },
    moduleModText:{
        color:'#fff'
    },
    moduleTitleText:{
        color:'#fff'
    },
    athVideoListItemBG:{
        width: (width-48)/2,
        height: (width-48)/1.5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        flex: 1
    },
    athVideoListItemBGImage:{
        borderRadius: 4
    },
    vimeoWebLoader:{
        width: width,
        height: width*0.56,
        alignItems: 'center',
        justifyContent: 'center',
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'#f5f5f5'
    },
    vimeoWebview:{
        width: width,
        height: width*0.56,
    },
    ATHRCategoryItemWrap:{
        backgroundColor:'#1C2127',
        padding: 16
    },
    athrciTitle:{
        color:'#fff'
    }, 
    athrciSubTitle:{
        color:'#fff'
    },
    athrciIconWrap:{
        width: 44
    },
    athrciIcon:{
        width: 44,
        height: 44
    }
});

export const commonStyles = StyleSheet.create({
    content: {
        backgroundColor: '#fff',
        paddingHorizontal: 16,
    },
    title:{
        fontSize: 16,
        color: '#4A4A4A',
        textAlign: 'center'
    }
});

export const homeStyles = StyleSheet.create({
    homeModuleItem:{
        width: (width-32)/2,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: "center",
        height: 160,
        padding: 10,
        justifyContent:'space-evenly',
    },
    modulesWrapper:{
        flex: 1,
        flexDirection: 'column',
    },
    moduleIcon:{

    },
    moduleTitle:{
        color:'#4A4A4A',
        fontSize: 14,
        textAlign:'center',
    }
});

export const controlPanelStyles = StyleSheet.create({
    profileWrapper:{
        height: 120,
        paddingHorizontal: 16,
        alignItems:'center',
        borderBottomColor: '#F1F1F1',
        borderBottomWidth: 1,
    }, 
    menuWrapper: {
        paddingHorizontal: 16,
    },
    listItem: {
        borderBottomWidth: 0,
        marginLeft: 10,
    },
    linkTitle:{
        fontWeight: "normal",
        fontSize: 14,
    },
    appIcon:{
        width: 56,
        height: 56,
    },
    userName:{
        color:'#4A4A4A',
        fontSize: 18
    },
    userEmail:{
        color:'#4A4A4A',
        fontSize: 14,
        opacity: 0.5
    }
});

export const ritualStyles = StyleSheet.create({
    bgImage:{
        width: width,
        height: 260
    },
    ritualsTitleWrap:{
        justifyContent: 'flex-end',
        alignItems:'center',
        height: 130,
        paddingHorizontal: 16
    },
    title: {
        color: '#fff',
        fontSize: 22,
        fontWeight:'bold'
    },
    subtitle:{
        color: '#fff',
        fontSize: 16, 
        textAlign:'center'
    }
});

export const missionsStyles = StyleSheet.create({
    itemWrap:{
        paddingHorizontal: 16,
        borderBottomColor:'#ddd',
        borderBottomWidth:1,
        backgroundColor:'#fff',
        borderRadius: 4,
    },
    contentWrap:{
        paddingLeft: 10
    },
    title: {
        fontSize: 16,
        color:'#333',
        fontWeight:'bold'
    },
    desc:{
        fontSize: 14,
        color:'#666'
    },
    text:{
        fontSize: 14,
        color:'#666',
        fontWeight: '600'
    }
})

export const emotionalButtonStyles = StyleSheet.create({
    image: {
        height: 145,
        width: (width - 32),
        borderRadius: 4
    },
    imageLoader:{
        height: 145,
        width: (width - 32),
        borderRadius: 4,
        backgroundColor:'#f5f5f5',
        alignItems:'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left:0,
        right:0,
        bottom: 0
    },
    textWrap:{
        padding: 8,
        borderWidth:1,
        borderLeftWidth: 4,
        borderColor:'#EC47A9',
        borderRadius: 4
    },
    playIcon:{
        width: 64,
        height:64,
    },
    playIconWrap:{
        position: 'absolute',
        width: 64,
        height:64,
        top:0,
        left:((width - 32) / 2) - 32,
        top: (145 / 2) - 32,
        right:0,
        bottom:0,
        zIndex: 10,
        backgroundColor:'#00000085',
        borderRadius: 32
    }
});

export const myAccountStyles = StyleSheet.create({
    profileHeaderWrap:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
    }
});