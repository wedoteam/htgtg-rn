import React, { Component } from 'react';
import { View, WebView, TouchableOpacity, ActivityIndicator } from 'react-native';
import {Container, Content, Text} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AppHeader from '../components/appheader'

import ATHRCategoryItem from '../components/modules/athrCategoryItem';

import {commonStyles, moduleStyles} from '../styles/app';

export class Module extends Component {

  constructor(){
    super();

    this.state = {
      webViewLoading: false
    }

    this.CategoryList = [
      {title: 'PART l', subtitle: 'Discover what you really want', id:0},
      {title: 'PART ll', subtitle: 'Unlock the secret to being Happy', id:1},
      {title: 'PART lll', subtitle: 'Master your Emotion & Actions', id:2},
      {title: 'PART llll', subtitle: 'Create meaningful Relationship', id:3},
      {title: 'PART V', subtitle: 'Achive core Confidence', id:4},
    ];
    this.goToCategory = this.goToCategory.bind(this);
  }

  goToCategory(item){
    this.props.navigation.navigate('Category');
  }

  render() {
    return (
      <Container>
          <AppHeader title='At-Home Retrat' navigation={this.props.navigation} showBack={true} />
          <Content>
            <Text style={commonStyles.title}>Mattew Hussey Retreat: At-home Program</Text>
            <View style={{height: 16}} />

            <View>
            <WebView
              source={{uri: 'https://player.vimeo.com/video/310350806'}}
              style={moduleStyles.vimeoWebview}
              onLoadEnd={() => {
                console.log('LOAD END');
                this.setState({webViewLoading : false});
              }}
              onLoadStart={() => {
                this.setState({webViewLoading : true});
              }}
            />

            {this.state.webViewLoading && 
              <View style={moduleStyles.vimeoWebLoader} >
                <ActivityIndicator size='small' /> 
              </View>
            }
            </View>

            
            <View style={{height: 16}} />
            
            {this.CategoryList.map((item) => {
              return (
                <TouchableOpacity key={item.id} activeOpacity={0.9} onPress={() => this.goToCategory(item)}>
                  <ATHRCategoryItem  title={item.title} subtitle={item.subtitle} />
                  <View style={{height: 4}} />
                </TouchableOpacity>
              )
            })}

          </Content>
      </Container>
    )
  }
}

export default connect(
    state=> ({
        auth: state.auth
    })
)(Module);