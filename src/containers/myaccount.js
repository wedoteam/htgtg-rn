import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Content, Text, Thumbnail, Grid, Col, Accordion, Right, Icon, Form, Item, Input, Label, Button } from 'native-base'
import { bindActionCreators, combineReducers } from 'redux'
import { connect } from 'react-redux'

import AppHeader from '../components/appheader'

import {myAccountStyles} from '../styles/app'

const logoDark = require('../assets/dark_logo.jpg');

class MyPrograms extends Component {
    render(){
        return (
            <View style={{paddingHorizontal: 16}}>
                <Text style={{fontSize: 16, color: '#333'}}>At-Home Retreat</Text>
                <View style={{height: 16}} />
                <Text style={{fontSize: 16, color: '#ff0000', fontWeight: '700'}}>Cancelled</Text>
                <View style={{height: 16}} />
            </View>
        )
    }
}

class AccountDetails extends Component {
    render(){
        return(
            <View>
                <Form>
                    <Item floatingLabel>
                        <Label>First Name</Label>
                        <Input value='John' />
                    </Item>
                    <Item floatingLabel>
                        <Label>Last Name</Label>
                        <Input value='Doe' />
                    </Item>
                    <Item floatingLabel>
                        <Label>Email</Label>
                        <Input value='johndoe@example.com' />
                    </Item>
                    <Item floatingLabel>
                        <Label>Username</Label>
                        <Input value='johndoe' />
                    </Item>
                    <View style={{paddingHorizontal: 16, alignItems:'flex-end', alignSelf:'flex-end'}}>
                        <View style={{height: 16}} />
                        <Button style={{backgroundColor:'#333'}} ><Text>Update Profile</Text></Button>
                        <View style={{height: 16}} />
                    </View>
                </Form>
            </View>
        )
    }
}

class BillingAddress extends Component {
    render(){
        return(
            <View>
                <Form>
                    <Item floatingLabel>
                        <Label>Address</Label>
                        <Input  />
                    </Item>
                    <Item floatingLabel>
                        <Label>City</Label>
                        <Input />
                    </Item>
                    <Item floatingLabel>
                        <Label>State</Label>
                        <Input  />
                    </Item>
                    <Item floatingLabel>
                        <Label>Zip Code</Label>
                        <Input  />
                    </Item>
                    <Item floatingLabel>
                        <Label>Country</Label>
                        <Input  />
                    </Item>
                    <View style={{paddingHorizontal: 16, alignItems:'flex-end', alignSelf:'flex-end'}}>
                        <View style={{height: 16}} />
                        <Button style={{backgroundColor:'#333'}} ><Text>Update Address</Text></Button>
                        <View style={{height: 16}} />
                    </View>
                </Form>
            </View>
        )
    }
}

class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    this.accordians = [
        {
            title: 'Account Details',
            id: 'Account_Details'
        },
        {
            title: 'Billing Address',
            id: 'Billing_Address'
        },
        {
            title: 'My Programs',
            id: 'My_Programs'
        }
    ]
    this._renderContent = this._renderContent.bind(this);
  }

  _renderHeRader(dataarray, expanded){
    return(
        <View style={{flexDirection: 'row', backgroundColor:'#fff', padding: 16,}} > 
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ textAlign: 'left', fontSize:16 }}>    
                    {dataarray.title}
                </Text>
            </View>
            <Right>
            {expanded
            ? <Icon style={{ fontSize: 18 }} name="ios-arrow-up" />
            : <Icon style={{ fontSize: 18 }} name="ios-arrow-down" />}
            </Right>
        </View>
    )
  }

  _renderContent(dataarray){
    return(
        <View>
            {dataarray.id == 'Account_Details' && 
                <AccountDetails />
            }
            {dataarray.id == 'Billing_Address' && 
                <BillingAddress />
            }
            {dataarray.id == 'My_Programs' && 
                <MyPrograms />
            }
        </View>
    )
  }

  render() {

    let user = this.props.auth.user;

    return (
      <Container>
          <AppHeader title="My Account" navigation={this.props.navigation} showBack={true} />
          <Content>
            
            <View style={{height:8}} />
            <Grid style={{paddingHorizontal:16, borderBottomColor:'#ddd', borderBottomWidth: 1}}>
                <Col style={{width: 96}}>
                    <Thumbnail source={logoDark} large />
                </Col>
                <Col>
                    <Text style={{fontSize: 18, color:'#333', fontWeight:'600'}} >{user.name}</Text>
                    <View style={{height:6}} />
                    <Text style={{fontSize: 16, color:'#666'}}>{user.email}</Text>
                    <View style={{height:6}} />
                    <Text style={{fontSize: 16, color:'#666'}}>Member Since : 12th Jan 2016</Text>
                    <View style={{height:16}} />
                </Col>
            </Grid>
            
            <Accordion
                dataArray={this.accordians}
                expanded={0}
                renderHeader={this._renderHeRader}
                renderContent={this._renderContent}
            />
            

          </Content>
      </Container>
    );
  }
}

export default connect(
    state => ({
        auth: state.auth
    })
)(MyAccount);
