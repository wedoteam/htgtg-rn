import React, { Component } from 'react'
import { View, ImageBackground, Image, SegmentedControlIOS, TouchableOpacity, KeyboardAvoidingView, Linking, Platform } from 'react-native'
import { Container, Content, Text, Grid, Col, Row, Segment, Button } from 'native-base'
import AppHeader from '../components/appheader';
import { videoDetailStyles } from '../styles/app';

import ArticleTitleView from '../components/common/articleTitleView'
// import VideoModel from '../components/common/videoModel'
import VideoPlayer from '../components/common/videoPlayer'

import ArticleContent from '../components/modules/articleContent'
import FooterForm from '../components/common/footerForm'
import AudioPlayer from '../components/common/audioPlayer'
// import AudioPlayeriOS from '../components/audioPlayer'

const pdfdwbtn = require('../assets/pdfdwbtn.png');

const bg1 = require('../assets/header/1.jpeg');
const bg2 = require('../assets/header/2.jpeg');
const bg3 = require('../assets/header/3.jpeg');
const bg4 = require('../assets/header/4.jpeg');
const bg5 = require('../assets/header/5.jpeg');
const bg6 = require('../assets/header/6.jpeg');
const bg7 = require('../assets/header/7.jpeg');
const bg8 = require('../assets/header/8.jpeg');
const bg9 = require('../assets/header/9.jpeg');
const bg10 = require('../assets/header/10.jpeg');
const bg11 = require('../assets/header/11.jpeg');
const bg12 = require('../assets/header/12.jpeg');
const bg13 = require('../assets/header/13.jpeg');

import { combineReducers } from 'redux';

export class DetailView extends Component {

  constructor(){
    super();
    this.state = {
      activeTab : 0,
      videoModelVisible: false,
      audioModelVisible: false,
      desc: '',
      showComments: false,
      showJournal: false, 
      isLoaded: false,
      data: '',
      moduleTitle: '',
      thumbnailUrl: '',
      videoUrl: '',
      video: ''
    }
    this._handleOnThumbClick = this._handleOnThumbClick.bind(this);
    this.closeVideoModel = this.closeVideoModel.bind(this);
    this.closeAudioModel = this.closeAudioModel.bind(this);

    this.bgImages = [
      bg1,bg2,bg3,bg4,bg5,bg6,bg7,bg8,bg9,bg10,bg11,bg12,bg13
    ]

  }

  _handleOnThumbClick(){
    let isAudio = this.state.data.item.audio;
    let isVideo = this.state.data.item.video;
    if(isAudio) {
      this.setState({ audioModelVisible: true });
    }
    if(isVideo){
      this.setState({ videoModelVisible: true });
    }
  }
  
  closeAudioModel(){
    this.setState({ audioModelVisible: false });
  }

  closeVideoModel(){
    this.setState({ videoModelVisible: false });
  }

  componentDidMount(){
    let params = this.props.navigation.state.params;
    this.setState({
      desc: params.desc,
      showJournal: params.showJournal,
      showComments: params.showComments,
      moduleTitle: params.moduleTitle,
      data: params.data,
      isLoaded: true
    });

    if(params.data.item.video){

      let videoId = 34824857;
      if (typeof params.data.item.videId !== 'undefined') {
        videoId = params.data.item.videId;
      }

      fetch(`https://player.vimeo.com/video/${videoId}/config`)
      .then(res => res.json())
      .then(res => {
          // console.log(res);
          this.setState({
            thumbnailUrl: res.video.thumbs['640'],
            videoUrl: res.request.files.hls.cdns[res.request.files.hls.default_cdn].url,
            video: res.video,
          })
        }
      );
    }

  }

  render() {
    var bgImage = this.bgImages[Math.floor(Math.random()*this.bgImages.length)];
    if(!this.state.isLoaded){
      return null
    }

    let isAudio = this.state.data.item.audio;
    let isVideo = this.state.data.item.video;

    let videoId = '34824857';
    let thumb = 'http://144.217.242.103/mhussey/34765628.jpg';
    if(isVideo){
      if (typeof this.state.data.item.videId !== 'undefined') {
        videoId = this.state.data.item.videId;
        thumb = 'http://144.217.242.103/mhussey/'+videoId+'.jpg';
      }
    }
    let audioUrl = 'http://144.217.242.103/mhussey/matthew-hussey-demo.mp3';
    if(isAudio){
      if (typeof this.state.data.item.audioUrl !== 'undefined') {
        audioUrl = this.state.data.item.audioUrl;
      }
    }

    let isPdf = false;
    let pdf = '';
    if(!isAudio && !isVideo){
      if (typeof this.state.data.item.pdf !== 'undefined') {
        isPdf = true;
        pdf = this.state.data.item.pdf;
      }
    }

    return (
      <Container>
        {isAudio ? 
          <View style={videoDetailStyles.headerBgMasterAudio}>
            <ImageBackground source={bgImage} style={videoDetailStyles.headerBg}>
              <AppHeader title={this.state.moduleTitle} showBack={true} navigation={this.props.navigation} translucent={true} />
              <ArticleTitleView title={this.state.data.item.title} isAudio={isAudio} isVideo={isVideo} onPress={this._handleOnThumbClick} image={thumb} /> 
            </ImageBackground>
          </View> : 
          <View style={videoDetailStyles.headerBgMaster}>
            <ImageBackground source={bgImage} style={videoDetailStyles.headerBg}>
              <AppHeader title={this.state.moduleTitle} showBack={true} navigation={this.props.navigation} translucent={true} />
              <ArticleTitleView title={this.state.data.item.title} isAudio={isAudio} isVideo={isVideo} onPress={this._handleOnThumbClick} image={thumb} /> 
            </ImageBackground>

            {isPdf && 
              <Grid>
                <Col style={{width: 146}}></Col>
                <Col style={{flexDirection: 'row', paddingTop: 8}}>
                  <TouchableOpacity activeOpacity={0.85} onPress={() => {
                    Linking.openURL(pdf);
                  }} >
                    <Image source={pdfdwbtn} style={{height: 32, width: 87, marginLeft: 8}} />
                  </TouchableOpacity>
                </Col>
              </Grid>
            }

          </View>
        }

        {isAudio && 
          <View>
            {Platform.OS == 'ios' ? 
              <AudioPlayer audioBook={audioUrl} /> : 
              <AudioPlayer audioBook={audioUrl} />  
            }
          </View>
        }
        
        <ArticleContent showComments={this.state.showComments} showJournal={this.state.showJournal} desc={this.state.desc} />
        <VideoPlayer 
          visible={this.state.videoModelVisible} 
          title={this.state.data.item.title} 
          showComments={this.state.showComments} 
          desc={this.state.desc} 
          showJournal={this.state.showJournal} 
          moduleTitle={this.state.moduleTitle} 
          closeVideoModel={this.closeVideoModel} 
          video={this.state.videoUrl} />

        {this.state.showComments && 
          <FooterForm placeholder="Add Your Comment..." buttonTitle="Submit" />
        }
        {this.state.showJournal && this.state.activeTab == 0 && 
          <FooterForm placeholder="Add New Journal..." buttonTitle="Submit" />
        }
        
      </Container>
    )
  }
}

export default DetailView