import React, { Component } from 'react';
import { View, ImageBackground, Image, StatusBar } from 'react-native';
import { Container, Grid, Row } from 'native-base';
import {styles} from '../styles/login';
const splashBG = require('../assets/splash-bg.png');
const logo = require('../assets/mh_logo.png');

export default class Splash extends Component {

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <ImageBackground  source={splashBG} style={styles.loginbg} >
          <Grid>
            <Row style={styles.logoContainer}>
              <Image source={logo} />
            </Row>
          </Grid>
        </ImageBackground>
      </Container>
    );
  }
}
