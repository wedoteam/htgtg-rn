import React, { Component } from 'react'
import { View, FlatList, Image, TouchableOpacity } from 'react-native'
import { Container, Content, Text, Grid, Col } from 'native-base'
import AppHeader from '../../components/appheader'

import { webinarList } from '../../data'
import { liveSessionsStyles } from '../../styles/app'

import VideoModel from '../../components/common/videoModel'

const dwbtn = require('../../assets/cheatsheetbtn.png');

class PastSessions extends Component {
  constructor(props) {
    super(props);
    this.state = {
        videoModelVisible: false,
        video: ''
    };
    this.closeVideoModel = this.closeVideoModel.bind(this);
  }

  closeVideoModel(){
      this.setState({
          videoModelVisible: false
      });
  }

  render() {
    return (
      <Container>
        <VideoModel visible={this.state.videoModelVisible} video={this.state.video} closeVideoModel={this.closeVideoModel} />
        <AppHeader title='Past Session Recordings' showBack={true} navigation={this.props.navigation}  />
        <FlatList
            data={webinarList}
            numColumns={1}
            ListHeaderComponent={() => {
                return (
                    <View style={{paddingHorizontal: 16}}>
                    <View style={{height: 12}} />
                    
                    <Text style={liveSessionsStyles.text}>Can’t make it to one of our live coaching sessions? Don’t worry – I’ll always save the recording for you here so you don’t miss a thing.</Text>
                    
                    <View style={{height: 12}} />
                    </View>
                )
            }}
            renderItem={(item) => {
                return (
                    <Grid key={item.index} style={liveSessionsStyles.pastSessionListItem}>
                        <Col>
                            <View style={{height: 16}} />
                            <TouchableOpacity onPress={() => {
                                console.log(item.item.video);
                                this.setState({ 
                                    videoModelVisible: true,
                                    video: item.item.video
                                });
                            }}>
                                <Text>{item.item.title}</Text>
                            </TouchableOpacity>
                            <View style={{height: 16}} />
                        </Col>
                        <Col style={liveSessionsStyles.dwBtnWrap}>
                                <Image source={dwbtn} style={liveSessionsStyles.dwBtn} /> 
                        </Col>
                    </Grid>
                )
            }}
        />
      </Container>
    );
  }
}

export default PastSessions;