import React, { Component } from 'react';
import { View, WebView } from 'react-native';
import { Container, Content, Text } from 'native-base'
import AppHeader from '../../components/appheader'
import SegmentControl from 'react-native-segment-controller';

import { liveSessionsStyles } from '../../styles/app'

class LiveSessions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0
    };
  }

  render() {
    return (
      <Container>
        <AppHeader title='Live Access (day of session)' showBack={true} navigation={this.props.navigation} />

        <View style={liveSessionsStyles.webviewVideoWrap} >
        <WebView 
          style={liveSessionsStyles.webviewVideo}
          source={{uri: 'https://player.vimeo.com/video/281600768'}}
        />
        </View>

        <View style={{height: 16}} />
        <SegmentControl
          values={['Q&A','Chat']}
          selectedIndex={this.state.activeTab}
          height={30}
          onTabPress={(item) => {
            this.setState({activeTab: item})
          }}
          borderRadius={5}
          activeTabStyle={{backgroundColor:'#333'}}
          tabTextStyle={{color:'#333'}}
          tabStyle={{borderColor:'#333'}}
          tabsContainerStyle={{paddingHorizontal: 16}}
        />
        <View style={{height: 16}} />
        
        {this.state.activeTab == 0 ? 
          <WebView 
            style={liveSessionsStyles.webviewChat}
            source={{uri: 'https://www.ustream.tv/qna/22211793?sitemode=1&site-embed=1'}}
          /> : 
          <WebView 
            style={liveSessionsStyles.webviewChat}
            source={{uri: 'https://www.ustream.tv/chat/22211793'}}
          />
        }
        

      </Container>
    );
  }
}

export default LiveSessions;