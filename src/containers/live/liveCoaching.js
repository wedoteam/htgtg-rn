import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content, Text, Button, Grid, Col, Item, Label, Input, Textarea } from 'native-base'
import AppHeader from '../../components/appheader'
import { liveSessionsStyles } from '../../styles/app'

class LiveCoaching extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <AppHeader title='Your Next Live Coaching With Matt' showBack={true} navigation={this.props.navigation} />
        <Content>
          <View style={{paddingHorizontal: 16}}>
            <Text style={liveSessionsStyles.text} >As one of my VIP Fast Track To Mr. Right members, you have access to a live, interactive coaching session with me personally.</Text>
            <View style={{height: 12}} />
            <Text style={liveSessionsStyles.text}>Our LIVE video coaching session will be held</Text>
            <View style={{height: 12}} />
            <Text style={liveSessionsStyles.textDate}>February 20th @ 11 am PT / 2 pm ET / 7 pm </Text>
            <View style={{height: 12}} />
            <Text style={liveSessionsStyles.text}>Don’t worry, If you can’t make it to any of our sessions, I’ll put up the recordings for you here.</Text>
            <View style={{height: 16}} />

            <Grid>
              <Col style={{paddingRight: 8}}>
                <Button block small style={{backgroundColor:'#333'}} onPress={() => {
                  this.props.navigation.navigate('LiveSessions');
                }}><Text>Go To Live Session</Text></Button>
              </Col>
              <Col style={{paddingLeft: 8}}>
                <Button block small style={{backgroundColor:'#333'}} onPress={() => {
                  this.props.navigation.navigate('PastSessions');
                }} ><Text>Past Live Sessions</Text></Button>
              </Col>
            </Grid>

            <View style={{height: 16}} />
            <Text style={liveSessionsStyles.text}>Ask me your burning questions below OR leave me your question at +1(310)-601-6926 to participate in a live call (if your question is selected.)</Text>

            <View style={{height: 16}} />

            <View>
              <Item floatingLabel >
                <Label>My Name *</Label>
                <Input />
              </Item>

              <View style={{height: 16}} />

              <Item floatingLabel >
                <Label>My Email *</Label>
                <Input />
              </Item>

              <View style={{height: 16}} />

              <Textarea  bordered placeholder="Here's My Question, Matt.." />
              
              <View style={{height: 16}} />

              <Button block style={{backgroundColor:'#333'}}><Text>Send Matt Your Question</Text></Button>
            </View>

          </View>

        </Content>
      </Container>
    );
  }
}

export default LiveCoaching;