import React, { Component } from 'react'
import { View, WebView } from 'react-native'
import { Container, Tab, Tabs, ScrollableTab, Text, TabHeading, Button, Content } from 'native-base'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import AppHeader from '../components/appheader'
import Module2List from '../components/common/module2List'
import {module2Styles} from '../styles/app'

import { module2Data } from '../data'

import { data } from '../modulesData'

export class Module2 extends Component {

  constructor(){
    super();
    this.state = {
      modulekey: '',
      moduledata: ''
    }
    this.tabs = [
        { title: 'Fast Track #1', id: 1 },
        { title: 'Fast Track #2', id: 2 },
        { title: 'Fast Track #3', id: 3 },
        { title: 'Fast Track #4', id: 4 },
        { title: 'Fast Track #5', id: 5 },
        { title: 'Fast Track #6', id: 6 },
        { title: 'Fast Track #7', id: 7 },
        { title: 'Fast Track #8', id: 8 },
        { title: 'Fast Track #9', id: 9 },
    ];
    this.navigateToDetailsScreen = this.navigateToDetailsScreen.bind(this);
  }

  componentWillReceiveProps(newProps){
    let module = newProps.navigation.state.params.module;
    this.setState({
      modulekey: module,
      moduledata: data[module]
    });
  }

  componentDidMount(){
    let module = this.props.navigation.state.params.module;
    this.setState({
      modulekey: module,
      moduledata: data[module]
    });
  }

  _rnderTabHeading(title){
      return(
          <TabHeading style={module2Styles.tabHeading} >
              <Text style={module2Styles.tabTitle} >{title}</Text>
          </TabHeading>
      )
  }

  navigateToDetailsScreen(item, moduleTitle) {
    // console.log(item);
    // console.log(moduleTitle);
    // console.log(item);
    this.props.navigation.navigate('DetailView', {
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      showComments: true,
      showJournal: false, 
      data: item,
      moduleTitle: moduleTitle
    });
  }

  render() {
    let moduleData = this.state.moduledata;
    console.log(moduleData);
    return (
      <Container>
          <AppHeader navigation={this.props.navigation} title={moduleData.title} showBack={true} />

          {moduleData.hascategories && 
            <Tabs renderTabBar={()=> <ScrollableTab />}
              tabBarUnderlineStyle={{backgroundColor:'#666', height:3}}
            >
            {moduleData.data.map((item) => {
                return(
                    <Tab heading={this._rnderTabHeading(item.title)} key={item.id}  >
                        <Module2List data={item.posts} moduleTitle={moduleData.title} navigateToDetailsScreen={this.navigateToDetailsScreen} />
                    </Tab>
                )
            })}
            </Tabs>
          }

          {!moduleData.hascategories && moduleData.hasposts && 
            <Module2List data={moduleData.data.posts} navigateToDetailsScreen={this.navigateToDetailsScreen} />
          }

          {!moduleData.hascategories && !moduleData.hasposts && 
            <Content>
            <View style={{paddingHorizontal: 16}}>

              <Text style={{color: '#333', fontSize: 18, textAlign:'center', fontWeight: 'bold',}}>What Men Want: 7 Secrets to Pull Him Closer Instead of Pushing Him Away</Text>

              <View style={{height: 16}} />

              <Text style={{color: '#333', fontSize: 14}}>Get ready…You’re about to finally discover What Men Want.</Text>
              <View style={{height: 16}} />
              <Text style={{color: '#333', fontSize: 14}}>I’ve distilled my essential 7 secrets of male psychology (from years as a top love-life strategist coaching both men and women) so that you can take quick action to draw any man you choose to you just like a magnet.</Text>
              <View style={{height: 16}} />
              <Text style={{color: '#333', fontSize: 14}}>In just a moment, I’m going to take you by the hand and walk you through those insanely tricky “early dating stages” so you can avoid the most common (and disastrous) mistakes almost every woman makes during this crucial time.</Text>
              <View style={{height: 16}} />
              <Text style={{color: '#333', fontSize: 14}}>You’ll discover what men are looking for in a woman they’ll commit to, the real reasons why men disappear (even when things seem like they’re going great), and my secret formula that guarantees HE’LL keep making the effort to see YOU again after every date. (Try it; you’ll be amazed at how quickly it works!)</Text>
              <View style={{height: 16}} />
              <Text style={{color: '#333', fontSize: 14}}>I’ll even reveal exactly how to hit the “sweet spot” of time spent together so he’ll become positively addicted to you.</Text>
              <View style={{height: 16}} />
              <Text style={{color: '#333', fontSize: 14}}>I know you’re as excited as I am, so let’s get started!</Text>
              <View style={{height: 16}} />
              <Button style={{backgroundColor: '#333'}} block><Text>Download Your Guide</Text></Button>
            </View>
            </Content>
          }
            
      </Container>
    )
  }
}

export default Module2