import React, { Component } from 'react';
import { View, ImageBackground, Image, StatusBar, KeyboardAvoidingView, SafeAreaView, Platform } from 'react-native';
import { Container, Content, Text, Grid, Col, Row, Toast } from 'native-base';
import { bindActionCreators } from 'redux';
import * as authActions from '../actions/auth'
import { connect } from 'react-redux';

import CommonLoader from '../components/commonloader';
import LoginForm from '../components/user/loginform';

import {styles} from '../styles/login';
const loginBG = require('../assets/login-bg.png');
const logo = require('../assets/mh_logo.png');

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.loginUser = this.loginUser.bind(this);
    this._gridContent = this._gridContent.bind(this);
  }

  componentDidMount(){
    // console.log(this.props.auth);
  }

  componentWillReceiveProps(newprops){
    if(newprops.auth.isAuth){

    }
  }

  loginUser(data){
    this.props.authactions.loginUser(data);
  }

  _gridContent(){
    return (
      <Grid>
        <Row style={styles.logoContainer}>
          <Image source={logo} />
        </Row>
        <Row style={styles.formContainer}>
            <LoginForm navigation={this.props.navigation} loginUser={this.loginUser} />
        </Row>
      </Grid>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <CommonLoader loading={this.props.auth.isLoading} />
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <ImageBackground resizeMode='cover' source={loginBG} style={styles.loginbg} >
          <SafeAreaView/>

          {
            Platform.OS == 'ios' ? 
            <KeyboardAvoidingView style={{flex: 1}} behavior='padding' enabled >
              {this._gridContent()}
            </KeyboardAvoidingView> : 
            <View style={{flex: 1}}>
              {this._gridContent()}
            </View>
          }

          
        </ImageBackground>
      </Container>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth
  }), 
  dispatch => ({
    authactions: bindActionCreators(authActions, dispatch)
  })
)(Login);
