import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Content, Text, Accordion, Right, Icon } from 'native-base'
import {connect} from 'react-redux'
import AppHeader from '../../components/appheader'
import CompletedMissions from '../../components/addons/completedMissions';
import PendingMissions from '../../components/addons/pendingMissions'
import CompleteMissionModel from '../../components/addons/completeMissionModel'

import { pendingMissions, completedMissions } from '../../data';

class Missions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modelVisible: false
    };
    this.accordion = [
      {
        title: 'Pending Missions',
        id:'pendingmissions',
      }, 
      {
        title: 'Completed Missions',
        id: 'completedmissions'
      }
    ];
    this._renderHeader = this._renderHeader.bind(this);
    this._renderContent = this._renderContent.bind(this);
    this.closeMissionModel = this.closeMissionModel.bind(this);
    this.openMissionModel = this.openMissionModel.bind(this);
  }

  componentDidMount(){
    console.log(this.props.auth);
  }

  _renderContent(dataarray){
    return (
      <View>
        {dataarray.id == 'pendingmissions' ? 
        <PendingMissions openMissionModel={this.openMissionModel} data={pendingMissions} /> : 
        <CompletedMissions data={completedMissions} />
        }
      </View>
    )
  }

  closeMissionModel(){
    this.setState({modelVisible: false});
  }

  openMissionModel(){
    this.setState({modelVisible: true});
  }

  _renderHeader(dataarray, expanded){
    return (
      <View
        style={{flexDirection: 'row', backgroundColor:'#fff', padding: 16,}}
      > 
        <View style={{ flexDirection: 'row' }}>
            <Text style={{ textAlign: 'left', fontSize:16 }}>    
                {dataarray.title}
            </Text>
        </View>
        <Right>
        {expanded
        ? <Icon style={{ fontSize: 18 }} name="ios-arrow-up" />
        : <Icon style={{ fontSize: 18 }} name="ios-arrow-down" />}
        </Right>
      </View>
    );
  }

  render() {
    let user = this.props.auth.user;
    return (
      <Container>
        <CompleteMissionModel isVisible={this.state.modelVisible} closeMissionModel={this.closeMissionModel} />
        <AppHeader title='Missions' navigation={this.props.navigation}  />
        <Content>
          <View style={{paddingHorizontal: 16, borderBottomColor:'#ddd', borderBottomWidth: 1}}>
            <Text style={{fontSize: 18, color:'#333333' }}>Hi, {user.name}</Text>
            <View style={{height: 8}} />
            <Text style={{fontSize: 14, color:'#333333' }}>It always helps to have a little push when we’re trying to stretch our comfort zone or make changes in our lives. So here are some quick missions to help give you momentum and get the ball rolling when you need to. </Text>
            <View style={{height: 8}} />
            <Text style={{fontSize: 14, color:'#333333' }}>Have fun and remember: Even overcoming the smallest challenge can make us realize the profound possibilities we have within our power!</Text>
            <View style={{height: 8}} />
            <Text style={{fontSize: 14, color:'#333333' }}>(Note: Before you can complete a mission, you'll need to write something in the "Describe it" box below, so remember to fill it in so you can move on to the next one!)</Text>
            <View style={{height: 16}} />
          </View>

          <Accordion 
            dataArray={this.accordion}
            expanded={0}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />

        </Content>
      </Container>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth
  })
)(Missions);