import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Container, Content, Text, Accordion, Icon, Right } from 'native-base'
import AppHeader from '../../components/appheader'

import { gratitudeSessions, journalData, journalData2 } from '../../data'
import JournalList from '../../components/common/journalList'

class GratitudeDiary extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    this._renderHeader = this._renderHeader.bind(this);
  }

  _renderHeader(dataarray, expanded) {
    return (
      <View
        style={{flexDirection: 'row', backgroundColor:'#fff', padding: 16,}}
      > 
        <View style={{ flexDirection: 'row' }}>
            <Text style={{ textAlign: 'left', fontSize:16 }}>    
                {dataarray.title}
            </Text>
        </View>
        <Right>
        {expanded
        ? <Icon style={{ fontSize: 18 }} name="ios-arrow-up" />
        : <Icon style={{ fontSize: 18 }} name="ios-arrow-down" />}
        </Right>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <AppHeader title='Gratitude Diary' navigation={this.props.navigation} />

        <Accordion
            dataArray={gratitudeSessions}
            icon="add"
            expandedIcon="remove"
            expanded={0}
            iconStyle={{ color: "green" }}
            expandedIconStyle={{ color: "red" }}
            renderHeader={this._renderHeader}
            renderContent={(item) => {
                return (
                    <JournalList data={journalData} />
                )
            }}
        />

      </Container>
    );
  }
}

export default GratitudeDiary;
