import React, { Component } from 'react'
import { View, FlatList, TouchableOpacity } from 'react-native'
import { Container, Content, Text } from 'native-base';
import AppHeader from '../../components/appheader';
import { atltitudeData } from '../../data';

import ATHRVideoItem from '../../components/modules/athrVideoItem';

class AltitudeSessions extends Component {

  constructor(){
    super();
    this.navigateToDetailsScreen = this.navigateToDetailsScreen.bind(this);
  }

  navigateToDetailsScreen(item){
    console.log(item);
    this.props.navigation.navigate('DetailView',{
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      showComments: false,
      showJournal: false,
      data: {
        item : {
          audio: false,
          video: true,
          title: 'Loren Ipsum dolor sit amet.'
        }
      },
      moduleTitle: 'ALTITUDE SESSIONS',
    });
  }

  render() {
    return (
      <Container>
        <AppHeader title='ALTITUDE SESSIONS' navigation={this.props.navigation} /> 
        <View style={{flex: 1}} >
          <FlatList 
            data = {atltitudeData}
            style={{}}
            numColumns={2}
            ListFooterComponent={() => {
              return (
                <View style={{height: 16}} /> 
              )
            }}
            ItemSeparatorComponent={() => {
              return(
                <View style={{height: 16}} /> 
              )
            }}
            renderItem = {(item) => {
              return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToDetailsScreen(item)}>
                  <ATHRVideoItem item={item.item} /> 
                </TouchableOpacity>
              )
            }}
          />
        </View>
      </Container>
    )
  }
}

export default AltitudeSessions