import React, { Component } from 'react'
import { View, ImageBackground } from 'react-native'
import { Container, Content, Text, Footer } from 'native-base'
import AppHeader from '../../components/appheader'
const ritualsBG = require('../../assets/rituals-bg.png');
import { ritualStyles } from '../../styles/app';
import LinearGradient from 'react-native-linear-gradient'

import FooterForm from '../../components/common/footerForm'
import RitualsList from '../../components/addons/ritualsList'

import { rituals } from '../../data'


class Rituals extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <Content>
          <ImageBackground source={ritualsBG} style={ritualStyles.bgImage}>
            <AppHeader title='Rituals' navigation={this.props.navigation} translucent={true} />
            <LinearGradient colors={['#00000000', '#00000075' , '#000000']} style={ritualStyles.ritualsTitleWrap} >
                <View style={{height: 16}} />
                <Text style={ritualStyles.title}>RITUALS CHEAT SHEET</Text>
                <View style={{height: 8}} />
                <Text style={ritualStyles.subtitle}>Remember! Structure makes real spontaneity possible.</Text>
                <View style={{height: 16}} />
            </LinearGradient>
          </ImageBackground>
          
          <View style={{paddingHorizontal: 16}}>
            <View style={{height: 16}} />
            <Text style={{color:'#666666', fontSize: 15}}>Our dreams aren’t built by the actions we do once a year, but by the actions we repeat every single day.</Text>
            <View style={{height: 8}} />
            <Text style={{color:'#666666', fontSize: 15}}>Use this space to jot down any ideas for new rituals you want to make to achieve your biggest goals. </Text>
            <View style={{height: 8}} />
            <Text style={{color:'#666666', fontSize: 15}}>Start small, keep them simple, and remember: the modest daily ritual you actually complete is 10x more powerful than the over-ambitious rituals you can’t stick to.</Text>
            <View style={{height: 16}} />
            
            <Text style={{color:'#333333', fontSize: 18}}>CREATE & ACTIVATE YOUR RITUALS</Text>
            <View style={{height: 12}} />
            <RitualsList rituals={rituals} />

          </View>
        </Content>

        <FooterForm placeholder='My Ritual is...' buttonTitle='Add Ritual'  />
      </Container>
    );
  }
}

export default Rituals;
