import React, { Component } from 'react';
import { View, FlatList, TouchableOpacity } from 'react-native';
import { Container, Content, Text } from 'native-base'

import AppHeader from '../../components/appheader'
import ATHRVideoItem from '../../components/modules/athrVideoItem'

import { gratitudeSessions } from "../../data";


class GratitudeSessions extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    this.navigateToDetailsScreen = this.navigateToDetailsScreen.bind(this);
  }

  navigateToDetailsScreen(item){
    this.props.navigation.navigate('DetailView',{
        desc: '',
        showComments: false,
        showJournal: true,
        showActivity: false,
        data: {
          item : {
            audio: false,
            video: true,
            title: 'Loren Ipsum dolor sit amet.'
          }
        },
        moduleTitle: 'Gratitude Sessions',
    });
  }

  render() {
    return (
      <Container>
        <AppHeader title='Gratitude Sessions' navigation={this.props.navigation} />

        <FlatList 
            data = {gratitudeSessions}
            style={{}}
            numColumns={2}
            ListFooterComponent={() => {
              return (
                <View style={{height: 16}} /> 
              )
            }}
            ItemSeparatorComponent={() => {
              return(
                <View style={{height: 16}} /> 
              )
            }}
            renderItem = {(item) => {
              return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToDetailsScreen(item)}>
                  <ATHRVideoItem item={item.item} /> 
                </TouchableOpacity>
              )
            }}
        />

      </Container>
    );
  }
}

export default GratitudeSessions;
