import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Text, Container, Content } from 'native-base'

import AppHeader from '../../components/appheader'
import EmotionalButtonText from '../../components/addons/emotionalbuttontext'
import EmotiobnalButtonImage from '../../components/addons/emotionalbuttonimage'
import EmotionalButtonVideo from '../../components/addons/emotionalbuttonvideo'

import ImageModel from '../../components/common/imagemodel'
import VideoModel from '../../components/common/videoModel'

import { emotionalButtons } from '../../data';

class EmotionalButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageModelVisible: false,
      videoModelVisible: false,
      image: '',
      video: ''
    };
    this.closeVideoModel = this.closeVideoModel.bind(this);
    this.closeImageModel = this.closeImageModel.bind(this);
    this.openImageModel = this.openImageModel.bind(this);
    this.openVideoModel = this.openVideoModel.bind(this);
  }

  closeVideoModel(){
    this.setState({videoModelVisible: false});
  }

  closeImageModel(){
    this.setState({imageModelVisible: false});
  }

  openImageModel(image){
    this.setState({
      imageModelVisible: true,
      image: image
    })
  }

  openVideoModel(video){
    this.setState({
      videoModelVisible: true,
      video: video
    })
  }

  render() {
    return (
      <Container>
        <AppHeader title='Emotional Buttons' navigation={this.props.navigation} />

        <FlatList
          data={emotionalButtons}
          numColumns={1}  
          ItemSeparatorComponent={() => {
            return (
              <View style={{height:12}} />
            )
          }}
          renderItem={(item) => {
            data = item.item;
            return(
              <View style={{paddingHorizontal: 16}}>
                {data.type == 'text' && 
                  <EmotionalButtonText data={data} />
                }
                {data.type == 'image' && 
                  <EmotiobnalButtonImage data={data} openImageModel={this.openImageModel} />
                }
                {data.type == 'video' && 
                  <EmotionalButtonVideo data={data} openVideoModel={this.openVideoModel} />
                }
              </View>
            )
          }}
        />
        <ImageModel visible={this.state.imageModelVisible} image={this.state.image} closeImageModel={this.closeImageModel} /> 
        <VideoModel visible={this.state.videoModelVisible} video={this.state.video} closeVideoModel={this.closeVideoModel} />
      </Container>
    );
  }
}

export default EmotionalButtons;
