import React, { Component } from 'react';
import { View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Content, Text, Grid, Col } from 'native-base'

import SegmentControl from 'react-native-segment-controller';

import AppHeader from '../../components/appheader'
import ArticleTitleView from '../../components/common/articleTitleView';
import VideoModel from '../../components/common/videoModel'
import JournalList from '../../components/common/journalList'
import WorkSheetList from '../../components/common/workSheetList'

import { journalData, activityWorksheetData } from '../../data';
import { videoDetailStyles } from '../../styles/app'

const bg = require('../../assets/video_bg.png');
const audiodwbtn = require('../../assets/audiodwbtn.png');
const pdfdwbtn = require('../../assets/pdfdwbtn.png');

class MorningVisualization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      videoModelVisible: false
    };
    this._handleOnThumbClick = this._handleOnThumbClick.bind(this);
    this.closeVideoModel = this.closeVideoModel.bind(this);
  }

  _handleOnThumbClick(){
    this.setState({ videoModelVisible: true });
  }
  
  closeVideoModel(){
    this.setState({ videoModelVisible: false });
  }

  render() {
    return (
      <Container>
        <VideoModel visible={this.state.videoModelVisible} closeVideoModel={this.closeVideoModel} video='https://player.vimeo.com/video/310350806' />
        <View style={videoDetailStyles.headerBgMaster}>
          <ImageBackground source={bg} style={videoDetailStyles.headerBg}>
            <AppHeader title='Morning Visualization' navigation={this.props.navigation} translucent={true} />
            <ArticleTitleView onPress={this._handleOnThumbClick} image='https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_1.jpg' /> 
          </ImageBackground>
          
          <Grid>
            <Col style={{width: 146}}></Col>
            <Col style={{flexDirection: 'row', paddingTop: 8}}>
              <TouchableOpacity activeOpacity={0.85} >
                <Image source={audiodwbtn} style={{height: 32, width: 96}} />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.85} >
                <Image source={pdfdwbtn} style={{height: 32, width: 87, marginLeft: 8}} />
              </TouchableOpacity>
            </Col>
          </Grid>

        </View>

        <View style={{flex: 1 }}>
          <View style={{height: 8}} />
          <SegmentControl
            values={['Journal','Activity Worksheet']}
            selectedIndex={this.state.activeTab}
            height={30}
            onTabPress={(item) => {
              this.setState({activeTab: item})
            }}
            borderRadius={5}
            activeTabStyle={{backgroundColor:'#333'}}
            tabTextStyle={{color:'#333'}}
            tabStyle={{borderColor:'#333'}}
            tabsContainerStyle={{paddingHorizontal: 16}}
          />
          <View style={{height: 8}} />
          {this.state.activeTab == 0 ? 
            <JournalList data={journalData} /> : 
            <WorkSheetList data={activityWorksheetData} />
          }
        </View>

      </Container>
    );
  }
}

export default MorningVisualization;
