import React, { Component } from 'react';
import { View, WebView, ActivityIndicator } from 'react-native';
import { Container, Content, Text, Grid, Row } from 'native-base'

import AppHeader from '../../components/appheader'

class Help extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoaded: false
    };
  }

  ActivityIndicatorLoadingView(){
    return (
      <ActivityIndicator />
    )
  }

  render() {
    return (
      <Container>
        <AppHeader title='Help' navigation={this.props.navigation} showBack={true} style={{flex: 1}} />

        {!this.state.isLoaded && <ActivityIndicator /> }

        <WebView
            style={{backgroundColor: '#eee'}}
            source={{uri: 'https://help.howtogettheguy.com/'}}
            onLoadEnd={() => {
                this.setState({isLoaded: true});
            }}
            startInLoadingState={false}
        /> 
      </Container>
    );
  }
}

export default Help;
