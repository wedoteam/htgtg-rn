import React, { Component } from 'react';
import { View, ImageBackground, Image, TouchableOpacity, StatusBar, KeyboardAvoidingView, SafeAreaView, Platform } from 'react-native';
import { Container, Content, Text, Grid, Col, Row } from 'native-base';
import { bindActionCreators } from 'redux';
import * as authActions from '../actions/auth'
import { connect } from 'react-redux'

import ForgotPasswordForm from '../components/user/forgotpasswordform';

import {styles} from '../styles/login';

const loginBG = require('../assets/login-bg.png');
const backIcon = require('../assets/left-arrow.png');

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.loginUser = this.loginUser.bind(this);
  }

  componentDidMount(){
    // console.log(this.props.auth);
  }

  loginUser(data){
    
  }

  _gridContent(){
    return (
      <Grid>
        <Row style={styles.logoForgotContainer}>
          <TouchableOpacity onPress={() => {
            this.props.navigation.goBack();
          }}>
            <Image source={backIcon} style={styles.backIcon} />
          </TouchableOpacity>
          <View style={{height:16}} />
          <Text style={styles.textheading}>Forgot Password ?</Text>
          <View style={{height:16}} />
          <Text style={styles.textsubheading}>Enter your email address to reset your password.</Text>
        </Row>
        <Row style={styles.formContainer}>
          <ForgotPasswordForm navigation={this.props.navigation} loginUser={this.loginUser} />
        </Row>
      </Grid>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#000" barStyle="light-content" /> 
        <ImageBackground  source={loginBG} style={styles.loginbg} >
          <SafeAreaView/>

          {Platform.OS == 'ios' ? 
            <KeyboardAvoidingView style={{flex: 1}} behavior='padding' enabled >
              {this._gridContent()}
            </KeyboardAvoidingView>: 
            <View style={{flex: 1}} >
              {this._gridContent()}
            </View>
          }

          
        </ImageBackground>
      </Container>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth
  }), 
  dispatch => ({
    authactions: bindActionCreators(authActions, dispatch)
  })
)(ForgotPassword);
