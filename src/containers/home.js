import React, { Component } from 'react';
import { View, FlatList, Image, TouchableOpacity } from 'react-native';
import {Container, Content, Text, Icon} from 'native-base';
import AppHeader from '../components/appheader';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import { commonStyles, homeStyles } from '../styles/app'

const icon_wmw = require('../assets/modules/wmw.png');
const icon_ktg = require('../assets/modules/ktg.png');
const icon_impact = require('../assets/modules/impact.png');
const icon_htttm = require('../assets/modules/htttm.png');
const icon_ghrbty = require('../assets/modules/ghrbty.png');
const icon_fftf = require('../assets/modules/fftf.png');
const icon_fttmr = require('../assets/modules/fttmr.png');
const icon_dr = require('../assets/modules/dr.png');
const icon_cm = require('../assets/modules/cm.png');
const icon_aam = require('../assets/modules/aam.png');


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    
    };
    this.modules = [
      {name: 'At-Home Retreat', icon: icon_dr, id: 0, module:'athomeretreat'},
      {name: 'What Men Want', icon: icon_wmw, id: 1, module:'wmw'},
      {name: 'Fast Track to Mr Right', icon: icon_fttmr, id: 2, module:'fttmr'},
      {name: 'Impact', icon: icon_impact, id: 3, module:'impact'},
      {name: 'Attract Any Man', icon: icon_aam, id: 4, module:'aam'}, 
      {name: 'How To Talk To Men (Secret Scripts)', icon: icon_htttm, id: 5, module:'htttm'},
      {name: 'Communication Master Class', icon: icon_cm, id: 6, module:'cm'},
      {name: 'Get Him Running Back To You', icon: icon_ghrbty, id: 7, module:'ghrbty'},
      {name: 'Keep The Guy', icon: icon_ktg, id: 8, module:'ktg'},
      {name: 'From Flirting To Forever Bonuses', icon: icon_fftf, id: 9, module:'fftfb'},
    ];
    this._handleModuleOnPress = this._handleModuleOnPress.bind(this);
  }

  _handleModuleOnPress(item){
    // console.log(item);
    if(item.module == 'athomeretreat'){
      this.props.navigation.navigate('Module');
    } else {
      this.props.navigation.navigate('Module2', {
        module: item.module
      });
    }
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title='My Programs'  />
        <Content style={commonStyles.content}>
          <View style={homeStyles.modulesWrapper}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => {
            this.props.navigation.navigate('LiveCoaching');
          }}>
            <View style={{backgroundColor:'#FCF77B', paddingHorizontal: 8, paddingVertical: 12, borderRadius: 4, borderColor:'#999', borderWidth:1}}>
              <Text style={{textAlign:'center'}}>For my "Fast Track to Mr.Right" VIPs only: Our next Live coaching session is February 20</Text>
            </View>
          </TouchableOpacity>
          <FlatList
            data={this.modules}
            numColumns={2}
            renderItem={({item}) => {
              return (
                <TouchableOpacity onPress={() => this._handleModuleOnPress(item)}>
                  <View key={item.id} style={homeStyles.homeModuleItem} >
                    <Image source={item.icon} style={homeStyles.moduleIcon} />
                    <Text style={homeStyles.moduleTitle}>{item.name}</Text>
                  </View>
                </TouchableOpacity>
              )
            }}
          />

          </View>
        </Content>
      </Container>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth
  })
)(Home);