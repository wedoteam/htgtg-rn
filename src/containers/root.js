import React, { Component } from 'react';
import { Root } from "native-base";
import { LoginContainer, MainContainer } from '../config/router';
import { bindActionCreators } from 'redux'
import * as authActions from '../actions/auth';
import { connect } from 'react-redux'
import Splash from '../containers/splash';


export class AppRoot extends Component {

  constructor(props){
    super(props);
  }

  componentDidMount(){
    this.props.authaction.restoreSession();
  }

  render() {
    if(this.props.auth.restoringSession){
        return <Splash/>;
    }
    if (this.props.auth.isAuth) {
        return (
            <Root>
                <MainContainer />
            </Root>
        );
    } else {
        return (
            <Root>
                <LoginContainer />
            </Root>
        );
    }
  }
}

export default connect(
    state => ({
        auth: state.auth
    }), 
    dispatch => ({
        authaction: bindActionCreators(authActions, dispatch)
    })
)(AppRoot);