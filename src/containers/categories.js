import React, { Component } from 'react'
import { View, FlatList, TouchableOpacity } from 'react-native'
import { Container, Content, Text } from 'native-base';
import AppHeader from '../components/appheader';
import { athdata } from '../data';

import ATHRVideoItem from '../components/modules/athrVideoItem';

class Category extends Component {

  constructor(){
    super();
    this.navigateToDetailsScreen = this.navigateToDetailsScreen.bind(this);
  }

  navigateToDetailsScreen(item){
    console.log(item);
    this.props.navigation.navigate('DetailView',{
      desc: '',
      showComments: false,
      showJournal: true,
      data: {
        item : {
          audio: false,
          video: true,
          title: 'Loren Ipsum dolor sit amet.'
        }
      },
      moduleTitle: 'At-Home Retreat',
    });
  }

  render() {
    return (
      <Container>
        <AppHeader title='At-Home Retreat' navigation={this.props.navigation} showBack={true} /> 
        <View style={{flex: 1}} >
          <FlatList 
            data = {athdata}
            style={{}}
            numColumns={2}
            ListFooterComponent={() => {
              return (
                <View style={{height: 16}} /> 
              )
            }}
            ItemSeparatorComponent={() => {
              return(
                <View style={{height: 16}} /> 
              )
            }}
            renderItem = {(item) => {
              return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToDetailsScreen(item)}>
                  <ATHRVideoItem item={item.item} /> 
                </TouchableOpacity>
              )
            }}
          />
        </View>
      </Container>
    )
  }
}

export default Category