export const athdata = [
    {
        title: "Lets create An Incredible Life You'll Love, Together",
        module: 'MODULE 1',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_1.jpg'
    },
    {
        title: "Your Action Plan To Stop Procrastination NOW",
        module: 'MODULE 2',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_2.jpg'
    },
    {
        title: "Wake up with 5x more energy Every Morning",
        module: 'MODULE 3',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_3.jpg'
    },
    {
        title: "Exercise: Goals That Make Your Life Worth Living",
        module: 'MODULE 4',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_4.jpg'
    },
    {
        title: "How To Become As Magnetic As Your Role Models",
        module: 'MODULE 5',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_5.jpg'
    },
    {
        title: "Life Begins At The End Of Your Comfort Zone",
        module: 'MODULE 6',
        img: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_6.jpg'
    }
];

export const webinarList = [
    {
        title: 'June 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'July 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'August 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'September 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'October 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'November 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'december 2018 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
    {
        title: 'January 2019 webinar',
        video: 'https://player.vimeo.com/video/281600768',
        downloadlink: ''
    },
]

export const emotionalButtons =[
    {
        type: 'image', 
        content: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_10.jpg',
        thumb: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_10.jpg'
    }, 
    {
        type: 'text',
        content: 'This is an Emotionsl Button with some randomText'
    },
    {
        type: 'text',
        content: 'This is an Emotionsl Button with some randomText. This is an Emotionsl Button with some randomText. This is an Emotionsl Button with some randomText. This is an Emotionsl Button with some randomText. This is an Emotionsl Button with some randomText. This is an Emotionsl Button with some randomText'
    },
    {
        type: 'video', 
        content: 'https://player.vimeo.com/video/310350806',
        thumb: 'https://members.howtogettheguy.com/wp-content/uploads/2016/12/module_6.jpg'
    }
]

export const atltitudeData = [
    {
        title: "The Life Changing magic of Gratitide",
        module: 'Altitude 1',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Past",
        module: 'Altitude 2',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Now",
        module: 'Altitude 3',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the People",
        module: 'Altitude 4',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Now",
        module: 'Altitude 5',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the People",
        module: 'Altitude 6',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    }
]

export const pendingMissions = [
    {
        title: 'MISSION 04',
        disc: 'Have a great conversation with a friend you haven’t spoken to in a while',
        id: 4
    },{
        title: 'MISSION 05',
        disc: 'Say “hi” to 2 new people this week and ask how their day is going or deliver a compliment.',
        id: 5
    },{
        title: 'MISSION 06',
        disc: 'Have a great conversation with a friend you haven’t spoken to in a while. Have a great conversation with a friend you haven’t spoken to in a while',
        id: 6
    },{
        title: 'MISSION 07',
        disc: 'Give yourself 15 minutes to stretch and work out your body before you start the day. (Look up home exercise videos on YouTube if you need a set program).',
        id: 7
    }
]

export const completedMissions = [
    {
        title: 'MISSION 01',
        disc: 'Have a great conversation with a friend you haven’t spoken to in a while',
        text:'Mission Completed',
        id: 1
    },{
        title: 'MISSION 02',
        disc: 'Say “hi” to 2 new people this week and ask how their day is going or deliver a compliment.',
        text:'Feeling Rejoice',
        id: 2
    },{
        title: 'MISSION 03',
        disc: 'Have a great conversation with a friend you haven’t spoken to in a while. Have a great conversation with a friend you haven’t spoken to in a while',
        text:'Had a Conversation with School friend. Feeling great.',
        id: 3
    }
]

export const rituals = [
    {
        title: 'Watching one hour of the AHR a day',
        isComplete: true
    },
    {
        title: 'Give Will a Compliment',
        isComplete: false
    },
]

export const gratitudeSessions = [
    {
        title: "The Life Changing magic of Gratitide",
        module: 'Gratitude 1',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Past",
        module: 'Gratitude 2',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Now",
        module: 'Gratitude 3',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the People",
        module: 'Gratitude 4',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the Now",
        module: 'Gratitude 5',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Gratitude for the People",
        module: 'Gratitude 6',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    }
]

export const journalsSessions = [
    {
        title: "The Life Changing magic of Journal",
        module: 'Journal 1',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Journal for the Past",
        module: 'Journal 2',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Journal for the Now",
        module: 'Journal 3',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Journal for the People",
        module: 'Journal 4',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Journal for the Now",
        module: 'Journal 5',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    },
    {
        title: "Journal for the People",
        module: 'Journal 6',
        img: 'https://members.howtogettheguy.com/wp-content/themes/gtgmember/assets/digitalretreat/img/module12-a1db8f7fb404cdddfa81881ab1b60f59.png'
    }
]

export const module2Data = [
    {
        title: '1. Discover What You Want in Love.', 
        desc: 'We can’t make progress in our love lives until we’ve discovered what it is we really want. Begin by identifying what your ideal outcome is for three months’ time, discover how to begin taking action in your love life.', 
        id: 0
    },
    {
        title: '2. Meet Amazing Men Now – Your Quick-Start Action Plan', 
        desc: 'Half the battle in meeting men is getting proactive, and the other half is location. Here you’ll discover a range of places you’ve never considered before to meet better quality guys.', 
        id: 1
    },
    {
        title: '3. How to Create Red-Hot Chemistry',
        desc: 'Half the battle in meeting men is getting proactive, and the other half is location. Here you’ll discover a range of places you’ve never considered before to meet better quality guys.',
        id: 2
    },
    {
        title:'4. Confessions of the World’s Biggest Player',
        desc: 'There was a time when Paul Janka may have been the world’s biggest player. Harvard educated, charming, tall, dark and handsome, he would meet women on the street of New York City.',
        id: 3
    },
    {
        title:'5. You Asked, Men Answer #1: A Roundtable Q+A with Guys',
        desc: 'Welcome to the Trainer Q&A calls! If you’ve ever wished you could be a fly on the wall during guys’ night, now’s your chance… Listen in as Michael, Raphael and I talk first date.',
        id: 4
    },
    {
        title: '6. Meet Amazing Men Now – Your Quick-Start Action Plan', 
        desc: 'Half the battle in meeting men is getting proactive, and the other half is location. Here you’ll discover a range of places you’ve never considered before to meet better quality guys.', 
        id: 5
    },
    {
        title:'7. Confessions of the World’s Biggest Player',
        desc: 'There was a time when Paul Janka may have been the world’s biggest player. Harvard educated, charming, tall, dark and handsome, he would meet women on the street of New York City.',
        id: 6
    },
]

export const module2Comments = [
    {
        user: 'John Doe',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        time: '3 Hours ago'
    },
    {
        user: 'Jane Doe',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        time: '3 Days ago'
    },
    {
        user: 'Jeen Doe',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        time: '4 Days ago'
    },
    {
        user: 'Doe Doe',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        time: '1 Month ago'
    },
    {
        user: 'John John',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        time: '1 Year ago'
    }
]

export const journalData = [
    {
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    }
];

export const activityWorksheetData = [
    {
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    },
    {
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
        time: '19 January 2019 4:00 AM'
    }
];